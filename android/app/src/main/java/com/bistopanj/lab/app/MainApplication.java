package com.bistopanj.lab.app;

import android.app.Application;
import android.util.Log;

import com.facebook.react.PackageList;
import com.facebook.hermes.reactexecutor.HermesExecutorFactory;
import com.facebook.react.bridge.JavaScriptExecutorFactory;
import com.facebook.react.ReactApplication;
import org.reactnative.camera.RNCameraPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.List;
import com.facebook.react.modules.i18nmanager.I18nUtil; // added by me
import com.microsoft.codepush.react.CodePush; // added by me for codepush

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    // added by me for codepush
    @Override
    protected String getJSBundleFile() {
      return CodePush.getJSBundleFile();
    }
    //

    @Override
    protected List<ReactPackage> getPackages() {
      @SuppressWarnings("UnnecessaryLocalVariable")
      List<ReactPackage> packages = new PackageList(this).getPackages();
      // Packages that cannot be autolinked yet can be added manually here, for example:
      // packages.add(new MyReactNativePackage());
      packages.add(new CodePush("MeBLB960P-Dia0Uy8catBOwmX935YTapLLMDk", MainApplication.this, BuildConfig.DEBUG)); // added by me for codepush
      return packages;
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    I18nUtil sharedI18nUtilInstance = I18nUtil.getInstance(); //<== added by me
    // sharedI18nUtilInstance.forceRTL(this, true); //<== added by me
    sharedI18nUtilInstance.allowRTL(this, true); //<== added by me

    SoLoader.init(this, /* native exopackage */ false);
  }
}
