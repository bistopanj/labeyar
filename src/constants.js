import { errorTypes, errorCodes } from './errors';

const appVersion = '0.2.6';

const timing = {
  minSplash: 1000,
  goToNextResult: 1000,
  changeDirectionTimeout: 3000,
};

const appParams = {
  phoneNumberMaxLen: 15,
  otpMaxLen: 8,
  otpMinLen: 4,
  serverBaseAddress: 'https://labsooz.herokuapp.com/',
  localStorageKey: 'USER_INFO',
  actionCenterHeight: 80,
  actionCenterWidthRatio: 0.35,
};

const localStorageKeys = {
  userInfo: 'USER_INFO',
  appLang: 'APP_LANG',
};

const langs = {
  en: { title: 'English', rtl: false, tag: 'en' },
  fr: { title: 'Français', rtl: false, tag: 'fr' },
  fa: { title: 'فارسی', rtl: true, tag: 'fa' },
  ar: { title: 'العربية', rtl: true, tag: 'ar' },
};

const fontFamilies = {
  en: { h: 'Ubuntu-Bold', p: 'OpenSans-Regular' },
  fr: { h: 'Ubuntu-Bold', p: 'OpenSans-Regular' },
  fa: { h: 'IRANYekanMobile-Bold', p: 'IRANYekanMobile-Light' },
  ar: { h: 'IRANYekanMobile-Bold', p: 'IRANYekanMobile-Light' },
};

export default {
  timing,
  appParams,
  errorTypes,
  errorCodes,
  localStorageKeys,
  langs,
  fontFamilies,
  appVersion,
};

export {
  timing,
  appParams,
  errorTypes,
  errorCodes,
  localStorageKeys,
  langs,
  fontFamilies,
  appVersion,
};
