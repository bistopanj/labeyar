import api from '../services/api.service';
import xhr from '../services/xhr.service';
import { appParams } from '../constants';
import { errorTypes, UnknownError, TypeError, AbortError, ValidationError } from '../errors';

const romanDigitLookup = {};
romanDigitLookup[String.fromCharCode(1776)] = '0';
romanDigitLookup[String.fromCharCode(1777)] = '1';
romanDigitLookup[String.fromCharCode(1778)] = '2';
romanDigitLookup[String.fromCharCode(1779)] = '3';
romanDigitLookup[String.fromCharCode(1780)] = '4';
romanDigitLookup[String.fromCharCode(1781)] = '5';
romanDigitLookup[String.fromCharCode(1782)] = '6';
romanDigitLookup[String.fromCharCode(1783)] = '7';
romanDigitLookup[String.fromCharCode(1784)] = '8';
romanDigitLookup[String.fromCharCode(1785)] = '9';
romanDigitLookup[String.fromCharCode(1632)] = '0';
romanDigitLookup[String.fromCharCode(1633)] = '1';
romanDigitLookup[String.fromCharCode(1634)] = '2';
romanDigitLookup[String.fromCharCode(1635)] = '3';
romanDigitLookup[String.fromCharCode(1636)] = '4';
romanDigitLookup[String.fromCharCode(1637)] = '5';
romanDigitLookup[String.fromCharCode(1638)] = '6';
romanDigitLookup[String.fromCharCode(1639)] = '7';
romanDigitLookup[String.fromCharCode(1640)] = '8';
romanDigitLookup[String.fromCharCode(1641)] = '9';

const persianDigitLookup = {
  0: String.fromCharCode(1776),
  1: String.fromCharCode(1777),
  2: String.fromCharCode(1778),
  3: String.fromCharCode(1779),
  4: String.fromCharCode(1780),
  5: String.fromCharCode(1781),
  6: String.fromCharCode(1782),
  7: String.fromCharCode(1783),
  8: String.fromCharCode(1784),
  9: String.fromCharCode(1785),
};

// This is the promisified version of XMLHttpResuest with a callback for progress report
const requestUploadImage = async ({ url, formData, progCB }, signal = null) => {
  try {
    const uploadResult = await xhr(url, {
      body: formData,
      method: 'Post',
      signal,
      onUploadProgress: progCB,
    });
    return uploadResult;
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'requestUploadImage request aborted by user.',
      });
    } else {
      throw new UnknownError({
        code: 9999,
        message: error.message || 'Error happened when trying to upload image to server.',
      });
    }
  }
};

const getUploadPermission = async (userInfo, signal = null) => {
  try {
    const { uploadUrl, formDataInfo } = await api.getUploadPermission(userInfo, signal);

    const formData = new FormData();
    Object.keys(formDataInfo).forEach(key => formData.append(key, formDataInfo[key]));
    return { formData, uploadUrl };
  } catch (error) {
    if (Object.keys(errorTypes).includes(error.type)) throw error;
    else {
      throw new UnknownError({
        code: 9999,
        message: error.message || 'something unknown caused problem in getUploadPermission.tools',
      });
    }
  }
};

// This method uploads an images and resolves with its public url.
const uploadPhoto = async ({ uri, progressCb, userInfo }, signal = null) => {
  try {
    const { uploadUrl, formData } = await getUploadPermission(userInfo, signal);
    // TODO: get the type of the file from uri and create a meaningful name for it.
    formData.append('file', { uri, type: 'image/jpg', name: 'upload.jpg' });
    const result = await requestUploadImage(
      { url: uploadUrl, formData, progCB: progressCb },
      signal,
    );
    return result.secure_url;
  } catch (error) {
    if (Object.keys(errorTypes).includes(error.type)) throw error;
    else {
      throw new UnknownError({
        code: 9999,
        message: error.message || 'something unknown caused problem in uploadPhoto.tools',
      });
    }
  }
};

const convertNumeralsToPersian = text => {
  if (typeof text !== 'string')
    throw new TypeError({ code: 1010, message: 'The input should be a string' });
  const converted = text.split('').reduce((acc, c) => {
    const convertedChar = c in persianDigitLookup ? persianDigitLookup[c] : c;
    const newAcc = `${acc}${convertedChar}`;
    return newAcc;
  }, '');
  return converted;
};

const convertNumeralsToRoman = text => {
  if (typeof text !== 'string')
    throw new TypeError({ code: 1010, message: 'The input should be a string' });
  const converted = text.split('').reduce((acc, c) => {
    const convertedChar = c in romanDigitLookup ? romanDigitLookup[c] : c;
    const newAcc = `${acc}${convertedChar}`;
    return newAcc;
  }, '');
  return converted;
};

const isPhoneNumber = text => {
  const pattern = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g;
  return pattern.test(text);
};

// This method converts the given phone number string to the standard format.
// Standard format has roman letter numbers.
// If the text is not a valid phone number, returns null.
const normalizePhoneNumber = text => {
  try {
    if (text.length < 10) {
      throw new ValidationError({
        code: 1008,
        message: 'Length of the input string is too short to be a valid phone number',
      });
    }

    const romanText = convertNumeralsToRoman(text);
    if (!isPhoneNumber(romanText)) {
      throw new ValidationError({
        code: 1009,
        message: 'The input string is not a valid phone number',
      });
    }

    const normalizedText = romanText.replace(/\D/g, '').replace(/^0+/, '');
    const minLen = /^98/g.test(normalizedText) ? 12 : 10;

    if (normalizedText.length >= minLen) return romanText;

    // const normalizedText = romanText.replace(/(?<!^)[^0-9]|^[^0-9+]/g, '');
    // const nakedText = normalizedText.replace(/^[0+]/, '');
    // const minLen = /^98/g.test(nakedText) ? 12 : 10;

    // if (nakedText.length >= minLen) return normalizedText;

    throw new ValidationError({
      code: 1008,
      message: 'Length of the input string is too short to be a valid phone number.',
    });
  } catch (error) {
    if (Object.keys(errorTypes).includes(error.type)) throw error;
    else {
      throw new UnknownError({
        code: 9999,
        message: error.message || 'something unknown caused problem in normalizePhoneNumber.tools',
      });
    }
  }
};

const normalizeOtp = text => {
  if (typeof text !== 'string')
    throw new TypeError({ code: 1010, message: 'The input should be a string' });
  if (text.length < appParams.otpMinLen)
    throw new ValidationError({
      code: 1011,
      message: 'Length of the input string is too short to be a valid one-time password',
    });

  try {
    const romanText = convertNumeralsToRoman(text);
    if (!Number.isNaN(romanText)) return romanText;
    throw new ValidationError({
      code: 1012,
      message: 'The input for otp should be numeric',
    });
  } catch (error) {
    if (Object.keys(errorTypes).includes(error.type)) throw error;
    else {
      throw new UnknownError({
        code: 9999,
        message: error.message || 'something unknown caused problem in normalizeOtp.tools',
      });
    }
  }
};

const polishNumber = (string, lang = 'fa') => {
  const polishMethod = {
    fa: convertNumeralsToPersian,
    ar: convertNumeralsToPersian,
  };

  return lang in polishMethod ? polishMethod[lang](string) : string;
};

// TODO: add more polishing (leading 0 etc.)
const polishPhoneNumber = phoneNumber => convertNumeralsToPersian(phoneNumber);

const compareVersions = (currentVer, newVer, major = false) => {
  // return true if newVer > currentVer
  const digitWeights = [100, 10, 1];
  const getVersionData = str => str.split('.').map(num => parseInt(num, 10));
  const currentVerData = getVersionData(currentVer);
  const newVerData = getVersionData(newVer);
  const comparison = newVerData.map((dig, index) => {
    if (dig > currentVerData[index]) return 1;
    if (dig < currentVerData[index]) return -1;
    return 0;
  });
  if (major) return newVerData[0] > currentVerData[0];
  const difference = comparison.reduce((acc, comp, index) => acc + comp * digitWeights[index], 0);
  return difference > 0;
};

export default {
  photo: {
    upload: uploadPhoto,
  },
  string: {
    normalizePhoneNumber,
    convertNumeralsToPersian,
    polishPhoneNumber,
    normalizeOtp,
    polishNumber,
    compareVersions,
  },
};

export { uploadPhoto, compareVersions, polishNumber, normalizeOtp, polishPhoneNumber };
