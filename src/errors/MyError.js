class MyError extends Error {
  constructor({ code, message }) {
    super(message);
    this.name = 'MyError';
    this.type = 'MyError';
    this.code = code;
  }
}

export default MyError;
