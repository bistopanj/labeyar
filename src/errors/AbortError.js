class AbortError extends Error {
  constructor(props) {
    if (typeof props === 'string') {
      super(props);
      this.name = 'AbortError';
      this.type = 'AbortError';
      this.message = props;
      this.code = 9998;
    } else {
      const { message, code } = props || {};
      super(message);
      this.name = 'AbortError';
      this.type = 'AbortError';
      this.message = message;
      this.code = code;
    }
  }
}

export default AbortError;
