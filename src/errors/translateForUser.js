import errorCodes from './errorCodes';

const translateForUser = error => {
  const { code = 9999 } = error || {};
  if (code in errorCodes) return `errors.${code}`;
  return 'errors.9999';
};

export default translateForUser;
