const errorTypes = {
  ServerError: 'ServerError',
  DeviceError: 'DeviceError',
  MyError: 'MyError',
  ValidationError: 'ValidationError',
  TypeError: 'TypeError',
  UnknownError: 'UnknownError',
  AbortError: 'AbortError',
};

export default errorTypes;
