import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import AppLoadingScreen from '../screens/AppLoadingScreen';
import AppNavigator from './AppNavigator';
import AuthNavigator from './AuthNavigator';

const topNavigator = createSwitchNavigator(
  {
    AppLoading: AppLoadingScreen,
    App: AppNavigator,
    Auth: AuthNavigator,
  },
  {
    initialRouteName: 'AppLoading',
  },
);

export default createAppContainer(topNavigator);
