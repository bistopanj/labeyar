import { createStackNavigator } from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import CameraScreen from '../screens/CameraScreen';
import GalleryScreen from '../screens/GalleryScreen';
import RecipientInfoScreen from '../screens/RecipientInfoScreen';
import SendResultScreen from '../screens/SendResultScreen';
import WaitingScreen from '../screens/WaitingScreen';

export default createStackNavigator(
  {
    Home: HomeScreen,
    Camera: CameraScreen,
    Gallery: GalleryScreen,
    RecipientInfo: RecipientInfoScreen,
    SendResult: SendResultScreen,
    Waiting: WaitingScreen,
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      header: null,
    },
  },
);
