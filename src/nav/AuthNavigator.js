import { createStackNavigator } from 'react-navigation';

import GetOtpScreen from '../screens/GetOtpScreen';
import LoginScreen from '../screens/LoginScreen';

export default createStackNavigator(
  {
    GetOtp: { screen: GetOtpScreen },
    Login: { screen: LoginScreen },
  },
  {
    initialRouteName: 'GetOtp',
    defaultNavigationOptions: {
      header: null,
    },
  },
);
