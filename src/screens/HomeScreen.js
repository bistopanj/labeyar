import React from 'react';
import { Card } from 'native-base';
import PropTypes from 'prop-types';
import { NavigationEvents } from 'react-navigation';
import CardItemText from '../components/CardItemText';
import Navbar from '../components/Navbar';
import PageWithButton from '../components/PageWithButton';
import { useAppContext } from '../contexts';

const HomeScreen = ({ navigation }) => {
  const { userInfo, t, clearResult, clearGallery } = useAppContext();

  const { primaryLab: { name = 'lab' } = {} } = userInfo || {};

  const openCamera = () => navigation.navigate('Camera');
  const clearApp = () => {
    clearResult();
    clearGallery();
  };

  return (
    <>
      <NavigationEvents onWillFocus={clearApp} />
      <Navbar title={name} />
      <PageWithButton
        buttonText="home.take-picture"
        scrollEnabled={false}
        onButtonPress={openCamera}
      >
        <Card>
          <CardItemText text={t('home.take-picture-guide')} />
        </Card>
      </PageWithButton>
    </>
  );
};

HomeScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default HomeScreen;
