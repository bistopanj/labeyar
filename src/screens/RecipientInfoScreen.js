import React, { useState } from 'react';
import { View, ScrollView } from 'react-native';
import { Card, CardItem } from 'native-base';
import PropTypes from 'prop-types';
import Navbar from '../components/Navbar';
import CardItemText from '../components/CardItemText';
import MobileNumberInput from '../components/MobileNumberInput';
import { useAppContext } from '../contexts';
import PageWithButton from '../components/PageWithButton';
import { Text } from '../components/RtlAware';
import styles from '../styles/styles';

const EnterClientMobileNumberCard = ({ onChange }) => {
  const { t } = useAppContext();
  return (
    <Card>
      <CardItemText text={t('result.enter-recipient-number')} />
      <CardItem>
        <MobileNumberInput onChange={onChange} autoFocus />
      </CardItem>
      <CardItem>
        <Text style={styles.secondaryText}>{t('result.will-receive-result')}</Text>
      </CardItem>
    </Card>
  );
};

EnterClientMobileNumberCard.propTypes = {
  onChange: PropTypes.func,
};

EnterClientMobileNumberCard.defaultProps = {
  onChange: () => ({}),
};

const RecipientInfoScreen = ({ navigation }) => {
  const [mobileNumber, setMobileNumber] = useState(null);
  const { t, addClientMobileNumber } = useAppContext();

  const proceed = () => {
    addClientMobileNumber(mobileNumber);
    navigation.navigate('SendResult');
  };

  return (
    <PageWithButton
      navbar={<Navbar title={t('result.recipient')} hasBack />}
      scrollEnabled={false}
      buttonDisabled={!mobileNumber}
      onButtonPress={proceed}
      avoidKeyboard
    >
      <View>
        <ScrollView contentContainerStyle={styles.centered}>
          <EnterClientMobileNumberCard onChange={setMobileNumber} />
        </ScrollView>
      </View>
    </PageWithButton>
  );
};

RecipientInfoScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default RecipientInfoScreen;
