import React, { useEffect, useState } from 'react';
import { View, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { Text } from '../components/RtlAware';
import AccessibleButton from '../components/AccessibleButton';
import { translateForUser } from '../errors';
import { useAppContext } from '../contexts';

const containerStyle = { flex: 1, justifyContent: 'center', padding: 10 };
const textStyle = { marginVertical: 40 };

const AppLoadingScreen = ({ navigation }) => {
  const [checkedLocalUserInfo, setCheckedLocalUserInfo] = useState(false);
  const [triedServerUpdate, setTriedServerUpdate] = useState(false);
  const [userInfoUpdatedFromServer, setUserInfoUpdatedFromServer] = useState(false);
  const { loadUserInfo, userInfo, showError, t, updateUserInfo } = useAppContext();
  const abortController = new AbortController();

  const fetchLocalUserInfo = async () => {
    try {
      await loadUserInfo();
    } catch (error) {
      showError(t(translateForUser(error)));
    } finally {
      setCheckedLocalUserInfo(true);
    }
  };

  useEffect(() => {
    fetchLocalUserInfo();
    return () => {
      abortController.abort();
    };
  }, []);

  const updateInfoFromServer = async () => {
    try {
      await updateUserInfo(abortController.signal);
      setUserInfoUpdatedFromServer(true);
    } catch (error) {
      showError(t(translateForUser(error)));
    } finally {
      setTriedServerUpdate(true);
    }
  };

  useEffect(() => {
    if (checkedLocalUserInfo) {
      if (!userInfo) navigation.navigate('Auth');
      else updateInfoFromServer();
    }
  }, [checkedLocalUserInfo]);

  useEffect(() => {
    if (triedServerUpdate && userInfoUpdatedFromServer) {
      navigation.navigate('App');
    }
  }, [triedServerUpdate]);

  const goToLogin = () => navigation.navigate('Auth');

  return (
    <View style={containerStyle}>
      <ActivityIndicator size="large" color="#990099" />
      {triedServerUpdate && !userInfoUpdatedFromServer && (
        <>
          <Text style={textStyle}>{t('login.cannot-get-user-info')}</Text>
          <AccessibleButton text={t('login.enter')} onPress={goToLogin} />
        </>
      )}
    </View>
  );
};

AppLoadingScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

AppLoadingScreen.defaultProps = {};

export default AppLoadingScreen;
