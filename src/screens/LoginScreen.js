import React, { useState, useEffect } from 'react';
import { StatusBar } from 'react-native';
import { Card, CardItem } from 'native-base';
import PropTypes from 'prop-types';
import PageWithButton from '../components/PageWithButton';
import { useAppContext } from '../contexts';
import { Text } from '../components/RtlAware';
import CardItemText from '../components/CardItemText';
import OtpInput from '../components/OtpInput';
import styles from '../styles/styles';
import { navigate } from '../services/nav.service';
import { translateForUser, errorTypes } from '../errors';

const EnterOtpCard = ({ onChange }) => {
  const { t } = useAppContext();
  return (
    <Card>
      <CardItemText text={t('login.enter-otp')} />
      <CardItem>
        <OtpInput onChange={onChange} autoFocus />
      </CardItem>
      <CardItem>
        <Text style={styles.secondaryText}>
          {t('login.not-received-otp')}
          <Text link onPress={() => navigate('GetOtp')}>
            {` ${t('login.try-again')}`}
          </Text>
        </Text>
      </CardItem>
    </Card>
  );
};

EnterOtpCard.propTypes = {
  onChange: PropTypes.func,
};

EnterOtpCard.defaultProps = {
  onChange: () => ({}),
};

const LoginScreen = ({ navigation }) => {
  const [isProcessing, setIsProcessing] = useState(false);
  const [otp, setOtp] = useState(null);
  const { showError, t, login: _login } = useAppContext();
  const {
    state: {
      params: { mobileNumber },
    },
  } = navigation;

  const abortController = new AbortController();

  const login = async () => {
    try {
      setIsProcessing(true);
      await _login({ mobileNumber, otp }, abortController.signal);
      setIsProcessing(false);
      navigation.navigate('AppLoading');
    } catch (error) {
      if (error.name !== errorTypes.AbortError) showError(t(translateForUser(error)));
      setIsProcessing(false);
    }
  };

  useEffect(() => {
    return () => {
      abortController.abort();
    };
  }, []);

  return (
    <>
      <StatusBar hidden />
      <PageWithButton
        scrollEnabled={false}
        buttonDisabled={!otp}
        isProcessing={isProcessing}
        onButtonPress={login}
        buttonText="login.enter"
        avoidKeyboard
      >
        <EnterOtpCard onChange={setOtp} />
      </PageWithButton>
    </>
  );
};

LoginScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    state: PropTypes.shape({
      params: PropTypes.shape({ mobileNumber: PropTypes.string.isRequired }).isRequired,
    }).isRequired,
  }).isRequired,
};

export default LoginScreen;
