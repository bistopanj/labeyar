import React, { useState, useRef, useEffect } from 'react';
import { View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { Card, CardItem } from 'native-base';
import CardItemText from '../components/CardItemText';
import { Text, H2 } from '../components/RtlAware';
import PageWithButton from '../components/PageWithButton';
import Navbar from '../components/Navbar';
import { useAppContext } from '../contexts';
import styles from '../styles/styles';
import tools from '../utils/tools';
import UploadManager from '../components/UploadManager';
import { translateForUser, errorTypes } from '../errors';
import { timing } from '../constants';

const imageSize = 150;

const uploaderContainerStyle = {
  height: imageSize,
  width: '100%',
  backgroundColor: 'black',
};
const PagesNumber = () => {
  const { t, lang, photos } = useAppContext();

  const pagesStr = tools.string.polishNumber(`${photos.length}`, lang);
  return (
    <CardItem style={{ flexDirection: 'column' }}>
      <Text style={styles.secondaryText}>{`${t('result.pages')} ${pagesStr}`}</Text>
    </CardItem>
  );
};

const ClientMobileNumber = () => {
  const { clientMobileNumber, lang } = useAppContext();
  return (
    <CardItem style={{ flexDirection: 'column' }}>
      <H2>{tools.string.polishNumber(clientMobileNumber || '09351269741', lang)}</H2>
    </CardItem>
  );
};

const UploadManagerContainer = ({ uploadManagerRef = null }) => {
  return (
    <CardItem>
      <View style={uploaderContainerStyle}>
        <UploadManager ref={uploadManagerRef} imageSize={imageSize} />
      </View>
    </CardItem>
  );
};

UploadManagerContainer.propTypes = {
  uploadManagerRef: PropTypes.shape({}),
};

UploadManagerContainer.defaultProps = {
  uploadManagerRef: null,
};

const ResultSummaryCard = ({ uploadManagerRef = null }) => {
  const { t } = useAppContext();
  return (
    <Card>
      <CardItemText text={t('result.will-send-to')} />
      <ClientMobileNumber />
      <UploadManagerContainer uploadManagerRef={uploadManagerRef} />
      <PagesNumber />
    </Card>
  );
};

ResultSummaryCard.propTypes = {
  uploadManagerRef: PropTypes.shape({}),
};

ResultSummaryCard.defaultProps = {
  uploadManagerRef: null,
};

const SendResultScreen = ({ navigation }) => {
  const [isProcessing, setIsProcessing] = useState(false);
  const uploadManager = useRef(null);
  const { t, showError, showSuccess, showInfo, submitResult, allUploaded } = useAppContext();
  const abortController = new AbortController();

  const handleSubmit = async () => {
    try {
      setIsProcessing(true);
      showInfo(t('result.uploading'));
      await uploadManager.current.uploadPhotos();
    } catch (error) {
      if (error.name !== errorTypes.AbortError) showError(t(translateForUser(error)));
    }
  };

  const sendResult = async () => {
    try {
      showSuccess(t('result.upload-succeeded'));
      await submitResult(abortController.signal);
      showSuccess(t('result.submitted'));
      setTimeout(() => {
        setIsProcessing(false);
        navigation.navigate('Home');
      }, timing.goToNextResult);
    } catch (error) {
      if (error.name !== errorTypes.AbortError) showError(t(translateForUser(error)));
      setIsProcessing(false);
    }
  };

  useEffect(() => {
    if (allUploaded) sendResult();
  }, [allUploaded]);

  useEffect(() => {
    return () => {
      abortController.abort();
    };
  }, []);

  return (
    <PageWithButton
      navbar={<Navbar title={t('result.final-confirm')} hasBack />}
      scrollEnabled={false}
      onButtonPress={handleSubmit}
      buttonDisabled={isProcessing}
      isProcessing={isProcessing}
      buttonText="result.send"
    >
      <View>
        <ScrollView contentContainerStyle={styles.centered}>
          <ResultSummaryCard uploadManagerRef={uploadManager} />
        </ScrollView>
      </View>
    </PageWithButton>
  );
};

SendResultScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default SendResultScreen;
