import React, { useState, useEffect } from 'react';
import { View, Linking, StatusBar, Keyboard } from 'react-native';
import PropTypes from 'prop-types';
import { Card, CardItem } from 'native-base';
import { Text, H3 } from '../components/RtlAware';
import LangSelector from '../components/LangSelector';
import PageWithButton from '../components/PageWithButton';
import { useAppContext } from '../contexts';
import styles from '../styles/styles';
import CardItemText from '../components/CardItemText';
import MobileNumberInput from '../components/MobileNumberInput';
import { translateForUser, errorTypes } from '../errors';

const IntroductionMessage = React.memo(
  () => {
    const { t } = useAppContext();
    return (
      <View style={styles.centered}>
        <View style={{ paddingVertical: 10 }}>
          <H3 style={styles.purpleText}>{t('login.send-results-electronically')}</H3>
        </View>
        <Text style={[styles.secondaryText, styles.smallText]}>{t('login.for-labs')}</Text>
      </View>
    );
  },
  () => true,
);

const MoreInfo = () => {
  const { t } = useAppContext();
  return (
    <View style={[styles.centered, { paddingHorizontal: 50 }]}>
      <View style={{ marginBottom: 14 }}>
        <Text style={styles.secondaryText}>
          {t('login.setup-lab')}
          <Text link onPress={() => Linking.openURL('https://bistopanj.com/lab')}>
            http://bistopanj.com/lab
          </Text>
        </Text>
      </View>
      <LangSelector />
    </View>
  );
};

const GetOtpCard = ({ onChange }) => {
  const { t } = useAppContext();
  return (
    <Card>
      <CardItemText text={t('login.enter-mobile-number')} />
      <CardItem>
        <MobileNumberInput onChange={onChange} />
      </CardItem>
    </Card>
  );
};

GetOtpCard.propTypes = {
  onChange: PropTypes.func,
};

GetOtpCard.defaultProps = {
  onChange: () => ({}),
};

const GetOtpScreen = ({ navigation }) => {
  const [isProcessing, setIsProcessing] = useState(false);
  const [mobileNumber, setMobileNumber] = useState(null);
  const { showError, t, getOtp } = useAppContext();

  const abortController = new AbortController();

  const askForOtp = async () => {
    try {
      setIsProcessing(true);
      Keyboard.dismiss();
      const otpData = await getOtp(mobileNumber, abortController.signal);
      setIsProcessing(false);
      navigation.navigate('Login', {
        mobileNumber,
        expiresInSeconds: otpData.expiresInSeconds,
      });
    } catch (error) {
      if (error.name !== errorTypes.AbortError) showError(t(translateForUser(error)));
      setIsProcessing(false);
    }
  };

  useEffect(() => {
    return () => {
      abortController.abort();
    };
  }, []);

  return (
    <>
      <StatusBar hidden />
      <PageWithButton
        scrollEnabled={false}
        buttonDisabled={!mobileNumber}
        isProcessing={isProcessing}
        onButtonPress={askForOtp}
        buttonText="login.confirm-mobile-number"
        avoidKeyboard
      >
        <IntroductionMessage />
        <GetOtpCard onChange={setMobileNumber} />
        <MoreInfo />
      </PageWithButton>
    </>
  );
};

GetOtpScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default GetOtpScreen;
