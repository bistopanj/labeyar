import React, { useState, useRef } from 'react';
import { View } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import PropTypes from 'prop-types';
import { Container, Button, Icon } from 'native-base';
import { RNCamera } from 'react-native-camera';
import { useAppContext } from '../contexts';
import CameraPermissionDialog from '../components/CameraPermissionDialog';
import CameraToolbar from '../components/CameraToolbar';
import { translateForUser } from '../errors';

const doNothing = () => ({});

const closeButtonStyle = {
  width: 80,
  height: 80,
  top: 30,
  position: 'absolute',
};
const closeButtonPosition = {
  'portrait-ltr': {
    end: 20,
  },
  'portrait-rtl': {
    start: 20,
  },
  'landscape-ltr': {
    start: 20,
  },
  'landscape-rtl': {
    end: 20,
  },
};

const CloseButton = ({ close = doNothing }) => {
  const { orientation, direction } = useAppContext();
  return (
    <Button
      transparent
      style={[closeButtonStyle, closeButtonPosition[`${orientation}-${direction}`]]}
      onPress={close}
    >
      <Icon active type="SimpleLineIcons" name="close" style={{ fontSize: 28, color: 'white' }} />
    </Button>
  );
};

CloseButton.propTypes = {
  close: PropTypes.func,
};

CloseButton.defaultProps = {
  close: doNothing,
};

const previewStyle = {
  position: 'absolute',
  left: 0,
  top: 0,
  right: 0,
  bottom: 0,
};

const CameraScreen = ({ navigation }) => {
  const [hasPermission, setHasPermission] = useState(null);
  const [cameraEnabled, setCameraEnabled] = useState(true);
  const [flashMode, setFlashMode] = useState(RNCamera.Constants.FlashMode.off);
  const [capturing, setCapturing] = useState(false);
  const [cameraType, setCameraType] = useState(RNCamera.Constants.Type.back);
  const camera = useRef(null);
  const { showError, t, addPhoto } = useAppContext();

  const enableCamera = () => setCameraEnabled(true);
  const disableCamera = () => setCameraEnabled(false);

  const closeCamera = () => {
    navigation.goBack(null);
  };

  const capture = async () => {
    try {
      const options = { fixOrientation: true };
      setCapturing(true);
      const photo = await camera.current.takePictureAsync(options);
      setCapturing(false);
      addPhoto(photo);
      navigation.navigate('Gallery');
    } catch (error) {
      showError(t(translateForUser(error)));
    }
  };

  if (hasPermission)
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <NavigationEvents onWillFocus={enableCamera} onDidBlur={disableCamera} />
          {cameraEnabled && (
            <RNCamera
              type={cameraType}
              flashMode={flashMode}
              style={previewStyle}
              ref={camera}
              captureAudio={false}
            />
          )}
        </View>
        <CloseButton close={closeCamera} />
        <CameraToolbar
          capturing={capturing}
          cameraType={cameraType}
          flashMode={flashMode}
          setFlashMode={setFlashMode}
          setCameraType={setCameraType}
          onCapture={capture}
        />
      </Container>
    );

  return <CameraPermissionDialog reportPermission={setHasPermission} />;
};

CameraScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

export default CameraScreen;
