import React, { useState, useEffect, useRef } from 'react';
import { View, Image, TouchableOpacity, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { Icon, Button } from 'native-base';
import PageWithButton from '../components/PageWithButton';
import Navbar from '../components/Navbar';
import { useAppContext } from '../contexts';
import { navigate } from '../services/nav.service';
import EditGalleryToolbar from '../components/EditGalleryToolbar';

const thumbnailHeight = 100;

const containerStyle = {
  'portrait-ltr': { flex: 1, flexDirection: 'column' },
  'portrait-rtl': { flex: 1, flexDirection: 'column' },
  'landscape-ltr': { flex: 1, flexDirection: 'row-reverse' },
  'landscape-rtl': { flex: 1, flexDirection: 'row' },
};
const thumbnailContainerStyle = {
  portrait: { height: thumbnailHeight, backgroundColor: 'rgba(0,0,0,0.85)' },
  landscape: { width: thumbnailHeight, backgroundColor: 'rgba(0,0,0,0.85)' },
};

const thumbnailStyle = { resizeMode: 'cover', width: '100%', height: '100%' };
const photoStyle = { resizeMode: 'contain', width: '100%', height: '100%' };
const buttonStyle = { width: 'auto', height: 'auto', padding: 0 };
const doNothing = () => ({});

const AddPhotoIcon = () => {
  const style = {
    aspectRatio: 1,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
  };

  const goToCamera = () => navigate('Camera');
  return (
    <View style={style}>
      <Button transparent onPress={goToCamera} style={buttonStyle}>
        <Icon type="FontAwesome" name="plus-square" style={{ fontSize: 36, color: 'white' }} />
      </Button>
    </View>
  );
};

const Thumbnail = ({ item, selected = false, onSelect = doNothing }) => {
  const selectedStyle = { borderWidth: 4, borderColor: 'white' };
  const unselectedStyle = { opacity: 0.7 };
  return (
    <TouchableOpacity onPress={onSelect}>
      <View style={[{ aspectRatio: 1 }, selected ? selectedStyle : unselectedStyle]}>
        <Image style={thumbnailStyle} source={{ uri: item.uri }} />
      </View>
    </TouchableOpacity>
  );
};

Thumbnail.propTypes = {
  item: PropTypes.shape({
    uri: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  }).isRequired,
  selected: PropTypes.bool,
  onSelect: PropTypes.func,
};

Thumbnail.defaultProps = {
  selected: false,
  onSelect: doNothing,
};

const GalleryScreen = ({ navigation }) => {
  const [selectedPhoto, setSelectedPhoto] = useState(null);
  const {
    photos,
    t,
    orientation,
    direction,
    removePhoto,
    movePhotoForward,
    movePhotoBack,
  } = useAppContext();
  const photoList = useRef(null);

  const scrollToEnd = () => photoList.current.scrollToEnd();

  const proceed = () => navigation.navigate('RecipientInfo');

  useEffect(() => {
    if (photos.length > 0) {
      setSelectedPhoto(photos[photos.length - 1]);
    }
  }, []);

  const handleContentSizeChange = () => {
    if (photos.length > 0) {
      setSelectedPhoto(photos[photos.length - 1]);
      scrollToEnd();
    } else {
      setSelectedPhoto(null);
    }
  };

  const moveSelectedPhotoBack = () => movePhotoBack(selectedPhoto);
  const moveSelectedPhotoForward = () => movePhotoForward(selectedPhoto);
  const removeSelectedPhoto = () => removePhoto(selectedPhoto);

  return (
    <>
      <Navbar title={t('result.test-result')} hasBack />
      <PageWithButton
        scrollEnabled={false}
        onButtonPress={proceed}
        buttonText="result.confirm-photos"
      >
        <View style={containerStyle[`${orientation}-${direction}`]}>
          <View style={{ backgroundColor: 'black', flex: 1 }}>
            {selectedPhoto && <Image style={photoStyle} source={{ uri: selectedPhoto.uri }} />}
            <EditGalleryToolbar
              moveBack={moveSelectedPhotoBack}
              moveForward={moveSelectedPhotoForward}
              remove={removeSelectedPhoto}
            />
          </View>
          <View style={thumbnailContainerStyle[orientation]}>
            <ScrollView
              ref={photoList}
              style={{ flex: 1 }}
              contentContainerStyle={{}}
              horizontal={orientation === 'portrait'}
              onContentSizeChange={handleContentSizeChange}
            >
              {photos.map(photo => (
                <Thumbnail
                  key={photo.id}
                  item={photo}
                  selected={photo.id === (selectedPhoto || {}).id}
                  onSelect={() => setSelectedPhoto(photo)}
                />
              ))}
              <AddPhotoIcon />
            </ScrollView>
          </View>
        </View>
      </PageWithButton>
    </>
  );
};

/*

<View style={thumbnailContainerStyle[orientation]}>
            <ScrollView
              ref={photoList}
              style={{ flex: 1, backgroundColor: 'blue' }}
              contentContainerStyle={{}}
              horizontal={orientation === 'portrait'}
              onContentSizeChange={handleContentSizeChange}
            >
              {photos.map(photo => (
                <Thumbnail
                  key={photo.id}
                  item={photo}
                  selected={photo.id === (selectedPhoto || {}).id}
                  onSelect={() => setSelectedPhoto(photo)}
                />
              ))}
              <AddPhotoIcon />
            </ScrollView>
          </View>

*/

GalleryScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default GalleryScreen;
