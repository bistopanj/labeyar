import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const WaitingScreen = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', padding: 10 }}>
      <ActivityIndicator size="large" color="#990099" />
    </View>
  );
};

export default WaitingScreen;
