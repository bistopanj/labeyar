import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
  bottomToolbar: {
    width: Math.min(Dimensions.get('window').width, Dimensions.get('window').height),
    position: 'absolute',
    height: 100,
    bottom: 0,
  },
  sideToolbar: {
    height: Math.min(Dimensions.get('window').width, Dimensions.get('window').height),
    position: 'absolute',
    width: 100,
  },
  alignCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  captureBtn: {
    width: 60,
    height: 60,
    borderWidth: 2,
    borderRadius: 60,
    borderColor: '#FFFFFF',
  },
  sideBtn: {
    color: 'white',
    fontSize: 34,
  },
  captureBtnActive: {
    backgroundColor: '#FFFFFF',
  },
});
