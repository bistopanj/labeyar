import { StyleSheet, Dimensions, Platform } from 'react-native';

const { width: winWidth, height: winHeight } = Dimensions.get('window');

const general = StyleSheet.create({
  secondaryText: {
    color: 'rgba(1,1,1,0.4)',
  },
  titleText: {
    color: 'white',
  },
  smallText: {
    fontSize: 12,
  },
  purpleText: {
    color: '#990099',
  },
  centered: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  splashContainer: {
    flex: 1,
    alignItems: 'center',
  },
  splashImage: {
    height: winHeight,
    width: winWidth,
    flex: 1,
    resizeMode: 'cover',
  },
  splashMessage: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    bottom: 80,
    color: 'white',
  },
  splashMessageText: {
    color: 'white',
    textAlign: 'center',
  },
  homePageContainer: {
    flex: 1,
    alignItems: 'center',
    paddingTop: Platform.OS === 'ios' ? 94 : 24,
  },
  mainContentContainer: {
    flex: 1,
  },
  topContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    // justifyContent: 'center',
  },
  actionCenter: {
    height: 80,
    justifyContent: 'center',
  },
  bigText: {
    fontSize: 20,
    fontFamily: 'IRANYekanMobile-ExtraBold',
    textAlign: 'center',
  },
  cardImage: {
    resizeMode: 'contain',
    marginVertical: 20,
    width: winWidth * 0.8,
    height: winHeight * 0.4,
  },
  resultPhoto: {
    resizeMode: 'contain',
    marginBottom: 20,
    width: winWidth * 0.8,
    height: winHeight * 0.4,
  },
  generalTextInput: {
    fontFamily: 'IRANYekanMobile-Light',
    fontSize: 14,
    textAlign: 'center',
  },
  headerButton: {
    paddingHorizontal: 20,
  },
  toastText: {
    fontSize: 12,
    textAlign: 'center',
  },
});

const landscape = StyleSheet.create({
  mainContentContainer: {
    flex: 1,
    flexDirection: 'row-reverse',
  },
});
const portrait = StyleSheet.create({
  mainContentContainer: {
    flex: 1,
  },
});

export default { ...general, landscape, portrait };
