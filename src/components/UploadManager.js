import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import PropTypes from 'prop-types';
import { AppContext } from '../contexts';
import ImageUploader from './ImageUploader';

const imageContainerStyle = { aspectRatio: 1 };
const imageStyle = { resizeMode: 'cover', width: '100%', height: '100%' };

const performSequenceAsync = async (func, inputs, acc) => {
  if (inputs.length > 0) {
    const input = inputs.shift();
    const output = await func(input);
    const newAcc = [...acc, output];
    return performSequenceAsync(func, inputs, newAcc);
  }
  return acc;
};

const UploadManager = class extends Component {
  constructor(props) {
    super(props);
    this.abortController = new AbortController();
    this.uploaders = {};
    this.scrollView = null;
    this.state = {};
    this.addScrollViewRef = this.addScrollViewRef.bind(this);
    this.scrollToIndex = this.scrollToIndex.bind(this);
    this.uploadPhotos = this.uploadPhotos.bind(this);
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  async uploadPhotos() {
    const { photos, addPublicUrl, markAllUploaded } = this.context;
    const photosCopy = photos.map((photo, index) => ({ ...photo, index }));
    const { uploaders, scrollToIndex } = this;

    const uploadPhoto = async photo => {
      const { publicUrl, id, index } = photo;
      if (!publicUrl) {
        scrollToIndex(index);
        const pubUrl = await uploaders[id].upload();
        addPublicUrl(photo, pubUrl);
      }
    };
    await performSequenceAsync(uploadPhoto, photosCopy, []);
    markAllUploaded();
    return true;
  }

  scrollToIndex(index) {
    const { imageSize } = this.props;
    this.scrollView.scrollTo({ x: imageSize * index, animated: true });
  }

  addScrollViewRef(el) {
    this.scrollView = el;
  }

  render() {
    const { photos } = this.context;
    return (
      <ScrollView style={{ flex: 1 }} horizontal ref={this.addScrollViewRef}>
        {photos.map(photo => (
          <View style={imageContainerStyle} key={`uploader-container-${photo.id}`}>
            <ImageUploader
              style={imageStyle}
              photo={photo}
              ref={element => {
                this.uploaders[photo.id] = element;
              }}
            />
          </View>
        ))}
      </ScrollView>
    );
  }
};

UploadManager.contextType = AppContext;

UploadManager.propTypes = {
  imageSize: PropTypes.number,
};

UploadManager.defaultProps = {
  imageSize: 150,
};

export default UploadManager;
