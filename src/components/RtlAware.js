import React from 'react';
import { Text, H1, H2, H3, Title, Subtitle, Input } from 'native-base';
import PropTypes from 'prop-types';
import { useLang } from '../contexts/lang';
import { findFontFamily } from '../services/lang.service';

const RtlAwareText = props => {
  const { direction = 'ltr', lang = 'en' } = useLang();
  const { children, style, link = false } = props;
  const font = findFontFamily(lang, 'p');
  const textStyle = { writingDirection: direction, fontFamily: font };
  if (link) textStyle.color = 'blue';

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Text {...props} style={[style, textStyle]}>
      {children}
    </Text>
  );
};

RtlAwareText.propTypes = {
  children: PropTypes.node,
  link: PropTypes.bool,
  style: PropTypes.oneOfType([PropTypes.shape({}), PropTypes.array]),
};

RtlAwareText.defaultProps = {
  children: null,
  link: false,
  style: {},
};

const RtlAwareTitle = props => {
  const { direction = 'ltr', lang = 'en' } = useLang();
  const { children, style } = props;
  const font = findFontFamily(lang, 'h');
  const textStyle = { writingDirection: direction, fontFamily: font };

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Title {...props} style={[style, textStyle]}>
      {children}
    </Title>
  );
};

RtlAwareTitle.propTypes = {
  children: PropTypes.node,
  style: PropTypes.oneOfType([PropTypes.shape({}), PropTypes.array]),
};

RtlAwareTitle.defaultProps = {
  children: null,
  style: {},
};

const RtlAwareSubtitle = props => {
  const { direction = 'ltr', lang = 'en' } = useLang();
  const { children, style } = props;
  const font = findFontFamily(lang, 'p');
  const textStyle = { writingDirection: direction, fontFamily: font };

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Subtitle {...props} style={[style, textStyle]}>
      {children}
    </Subtitle>
  );
};

RtlAwareSubtitle.propTypes = {
  children: PropTypes.node,
  style: PropTypes.oneOfType([PropTypes.shape({}), PropTypes.array]),
};

RtlAwareSubtitle.defaultProps = {
  children: null,
  style: {},
};

const RtlAwareH1 = props => {
  const { direction = 'ltr', lang = 'en' } = useLang();
  const { children, style, link = false } = props;
  const font = findFontFamily(lang, 'h');
  const textStyle = { writingDirection: direction, fontFamily: font };
  if (link) textStyle.color = 'blue';

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <H1 {...props} style={[style, textStyle]}>
      {children}
    </H1>
  );
};

RtlAwareH1.propTypes = {
  children: PropTypes.node,
  link: PropTypes.bool,
  style: PropTypes.shape({}),
};

RtlAwareH1.defaultProps = {
  children: null,
  link: false,
  style: {},
};

const RtlAwareH2 = props => {
  const { direction = 'ltr', lang = 'en' } = useLang();
  const { children, style, link = false } = props;
  const font = findFontFamily(lang, 'h');
  const textStyle = { writingDirection: direction, fontFamily: font };
  if (link) textStyle.color = 'blue';

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <H2 {...props} style={[style, textStyle]}>
      {children}
    </H2>
  );
};

RtlAwareH2.propTypes = {
  children: PropTypes.node,
  link: PropTypes.bool,
  style: PropTypes.shape({}),
};

RtlAwareH2.defaultProps = {
  children: null,
  link: false,
  style: {},
};

const RtlAwareH3 = props => {
  const { direction = 'ltr', lang = 'en' } = useLang();
  const { children, style, link = false } = props;
  const font = findFontFamily(lang, 'h');
  const textStyle = { writingDirection: direction, fontFamily: font };
  if (link) textStyle.color = 'blue';

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <H3 {...props} style={[style, textStyle]}>
      {children}
    </H3>
  );
};

RtlAwareH3.propTypes = {
  children: PropTypes.node,
  link: PropTypes.bool,
  style: PropTypes.shape({}),
};

RtlAwareH3.defaultProps = {
  children: null,
  link: false,
  style: {},
};

const RtlAwareInput = props => {
  const { direction = 'ltr', lang = 'en' } = useLang();
  const { children, style } = props;
  const font = findFontFamily(lang, 'p');
  const textStyle = { writingDirection: direction, fontFamily: font };

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Input {...props} style={[style, textStyle]}>
      {children}
    </Input>
  );
};

RtlAwareInput.propTypes = {
  children: PropTypes.node,
  style: PropTypes.shape({}),
};

RtlAwareInput.defaultProps = {
  children: null,
  style: {},
};

export {
  RtlAwareText as Text,
  RtlAwareH1 as H1,
  RtlAwareH2 as H2,
  RtlAwareH3 as H3,
  RtlAwareInput as Input,
  RtlAwareTitle as Title,
  RtlAwareSubtitle as Subtitle,
};
