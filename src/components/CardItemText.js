import React from 'react';
import { CardItem } from 'native-base';
import PropTypes from 'prop-types';
import { Text } from './RtlAware';

const CardItemText = ({ text = '' }) => {
  return (
    <CardItem>
      <Text>{text}</Text>
    </CardItem>
  );
};

CardItemText.propTypes = {
  text: PropTypes.string,
};

CardItemText.defaultProps = {
  text: '',
};

export default CardItemText;
