import React from 'react';
import { KeyboardAvoidingView, View, Platform } from 'react-native';
import { Content, Container } from 'native-base';
import PropTypes from 'prop-types';
import styles from '../styles/styles';
import { useAppContext } from '../contexts';
import AccessibleButton from './AccessibleButton';

const containerStyle = {
  'portrait-ltr': { flex: 1 },
  'portrait-rtl': { flex: 1 },
  'landscape-ltr': { flex: 1, flexDirection: 'row' },
  'landscape-rtl': { flex: 1, flexDirection: 'row-reverse' },
};

const doNothing = () => ({});

const PageContainer = ({
  children,
  avoidKeyboard = false,
  scrollEnabled = true,
  navbar = null,
}) => {
  const { orientation, direction } = useAppContext();
  if (avoidKeyboard)
    return (
      <Container>
        {navbar}
        <KeyboardAvoidingView
          style={{ backgroundColor: 'transparent', flex: 1 }}
          behavior="padding"
          enabled={Platform.OS === 'ios' || !navbar}
        >
          <Content
            contentContainerStyle={containerStyle[`${orientation}-${direction}`]}
            scrollEnabled={scrollEnabled}
            enableAutomaticScroll={false}
            enableOnAndroid={false}
            disableKBDismissScroll
          >
            {children}
          </Content>
        </KeyboardAvoidingView>
      </Container>
    );
  return (
    <Container>
      {navbar}
      <Content
        contentContainerStyle={containerStyle[`${orientation}-${direction}`]}
        scrollEnabled={scrollEnabled}
        enableAutomaticScroll={false}
        enableOnAndroid={false}
        disableKBDismissScroll
      >
        {children}
      </Content>
    </Container>
  );
};

PageContainer.propTypes = {
  children: PropTypes.node,
  avoidKeyboard: PropTypes.bool,
  scrollEnabled: PropTypes.bool,
  navbar: PropTypes.element,
};

PageContainer.defaultProps = {
  children: null,
  avoidKeyboard: false,
  scrollEnabled: true,
  navbar: null,
};

const PageWithButton = ({
  children,
  buttonText,
  isProcessing = false,
  buttonDisabled = false,
  onButtonPress = doNothing,
  avoidKeyboard = false,
  scrollEnabled = true,
  hideButton = false,
  navbar = null,
}) => {
  const { t } = useAppContext();
  return (
    <PageContainer avoidKeyboard={avoidKeyboard} scrollEnabled={scrollEnabled} navbar={navbar}>
      <View style={styles.topContainer}>{children}</View>
      {!hideButton && (
        <AccessibleButton
          info
          text={t(buttonText)}
          isProcessing={isProcessing}
          disabled={buttonDisabled}
          onPress={onButtonPress}
        />
      )}
    </PageContainer>
  );
};

PageWithButton.propTypes = {
  children: PropTypes.node,
  buttonText: PropTypes.string,
  isProcessing: PropTypes.bool,
  buttonDisabled: PropTypes.bool,
  avoidKeyboard: PropTypes.bool,
  scrollEnabled: PropTypes.bool,
  hideButton: PropTypes.bool,
  onButtonPress: PropTypes.func,
  navbar: PropTypes.element,
};

PageWithButton.defaultProps = {
  children: null,
  buttonText: 'general.confirm',
  isProcessing: false,
  buttonDisabled: false,
  avoidKeyboard: false,
  scrollEnabled: true,
  hideButton: false,
  onButtonPress: doNothing,
  navbar: null,
};

export default PageWithButton;
