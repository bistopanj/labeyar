import React, { useState } from 'react';
import { View } from 'react-native';
import { Icon, Button } from 'native-base';
import PropTypes from 'prop-types';
import { Text } from './RtlAware';
import { useAppContext } from '../contexts';

const buttonStyle = { width: 'auto', height: 'auto', padding: 0 };
const bottomBarStyle = {
  position: 'absolute',
  bottom: 0,
  right: 0,
  left: 0,
  minHeight: 75,
  justifyContent: 'center',
  alignItems: 'center',
};

const containerStyle = {
  position: 'absolute',
  bottom: 0,
  right: 0,
  left: 0,
  height: '100%',
  backgroundColor: 'rgba(0,0,0,0.4)',
  justifyContent: 'center',
};

const actionBarContainerStyle = { flex: 1, flexDirection: 'row' };
const actionsStyle = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
};

const doNothing = () => ({});

const ModifyButton = ({ onPress = doNothing }) => {
  const { t } = useAppContext();
  return (
    <View style={bottomBarStyle}>
      <Button bordered light onPress={onPress}>
        <Text>{t('general.edit')}</Text>
      </Button>
    </View>
  );
};

ModifyButton.propTypes = {
  onPress: PropTypes.func,
};

ModifyButton.defaultProps = {
  onPress: doNothing,
};

const IconButton = ({ type = 'FontAwesome', name, onPress = doNothing }) => {
  return (
    <View style={actionsStyle}>
      <Button transparent style={buttonStyle} onPress={onPress}>
        <Icon type={type} name={name} style={{ fontSize: 36, color: 'white' }} />
      </Button>
    </View>
  );
};

IconButton.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  onPress: PropTypes.func,
};

IconButton.defaultProps = {
  type: 'FontAwesome',
  onPress: doNothing,
};

const EditGalleryToolbar = ({
  moveBack = doNothing,
  moveForward = doNothing,
  remove = doNothing,
}) => {
  const [mode, setMode] = useState('view');
  const { direction } = useAppContext();

  const switchToEdit = () => setMode('edit');
  const switchToView = () => setMode('view');

  if (mode === 'view') return <ModifyButton onPress={switchToEdit} />;
  return (
    <View style={containerStyle}>
      <View style={actionBarContainerStyle}>
        <IconButton
          onPress={moveBack}
          name={direction === 'ltr' ? 'caret-square-o-left' : 'caret-square-o-right'}
        />
        <IconButton onPress={remove} type="FontAwesome5" name="trash-alt" />
        <IconButton
          onPress={moveForward}
          name={direction === 'ltr' ? 'caret-square-o-right' : 'caret-square-o-left'}
        />
      </View>
      <View style={bottomBarStyle}>
        <IconButton name="check-square-o" onPress={switchToView} />
      </View>
    </View>
  );
};

EditGalleryToolbar.propTypes = {
  moveBack: PropTypes.func,
  moveForward: PropTypes.func,
  remove: PropTypes.func,
};

EditGalleryToolbar.defaultProps = {
  moveBack: doNothing,
  moveForward: doNothing,
  remove: doNothing,
};

export default EditGalleryToolbar;
