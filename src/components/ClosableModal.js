import React from 'react';
import { Modal, TouchableWithoutFeedback, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  pageContainer: {
    display: 'flex',
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    borderColor: '#E5E8E8',
    borderWidth: 8,
    backgroundColor: 'white',
    minWidth: '70%',
    maxWidth: '90%',
    minHeight: 350,
    // height: 'auto',
    padding: 16,
  },
});

const doNothing = () => ({});

const ClosableModal = props => {
  const { children, close = doNothing, visible = false, boxStyle = {} } = props;
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Modal visible={visible} transparent onRequestClose={close} {...props}>
      <TouchableWithoutFeedback onPress={close}>
        <View style={styles.pageContainer}>
          <TouchableWithoutFeedback>
            <View style={[styles.boxContainer, boxStyle]}>{children}</View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

ClosableModal.propTypes = {
  children: PropTypes.node,
  close: PropTypes.func,
  visible: PropTypes.bool,
  boxStyle: PropTypes.shape({}),
};

ClosableModal.defaultProps = {
  children: null,
  close: doNothing,
  visible: false,
  boxStyle: {},
};

export default ClosableModal;
