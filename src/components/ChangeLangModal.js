import React, { useState } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { Button, ListItem, Left, Right, Radio } from 'native-base';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import ClosableModal from './ClosableModal';
import { Text } from './RtlAware';
import { langs } from '../constants';
import { useLang } from '../contexts/lang';

const styles = StyleSheet.create({
  title: {
    paddingBottom: 8,
  },
  content: {
    flex: 1,
  },
  langsContainer: {
    marginBottom: 12,
  },
});

const RtlAwareText = props => {
  const { direction = 'ltr', children, style = {} } = props;
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Text {...props} style={[style, { direction, writingDirection: direction }]}>
      {children}
    </Text>
  );
};

RtlAwareText.propTypes = {
  direction: PropTypes.string,
  children: PropTypes.node,
  style: PropTypes.object, // eslint-disable-line react/forbid-prop-types
};

RtlAwareText.defaultProps = {
  direction: 'rtl',
  children: null,
  style: {},
};

const doNothing = () => ({});

const LangItem = ({ lang, selectedLang, onSelect }) => (
  <ListItem
    onPress={() => {
      onSelect(lang);
    }}
  >
    <Left>
      <Text>{langs[lang].title}</Text>
    </Left>
    <Right>
      <Radio
        selected={lang === selectedLang}
        onPress={() => {
          onSelect(lang);
        }}
      />
    </Right>
  </ListItem>
);

LangItem.propTypes = {
  onSelect: PropTypes.func,
  lang: PropTypes.string,
  selectedLang: PropTypes.string,
};

LangItem.defaultProps = {
  onSelect: doNothing,
  lang: 'en',
  selectedLang: 'en',
};

const LangList = ({ onSelect = doNothing, selectedLang = 'en' }) => {
  return (
    <ScrollView style={styles.langsContainer}>
      {Object.keys(langs).map(lang => (
        <LangItem
          key={`select-lang-${lang}`}
          onSelect={onSelect}
          lang={lang}
          selectedLang={selectedLang}
        />
      ))}
    </ScrollView>
  );
};

LangList.propTypes = {
  onSelect: PropTypes.func,
  selectedLang: PropTypes.string,
};

LangList.defaultProps = {
  onSelect: doNothing,
  selectedLang: 'en',
};

const ChangeLangModal = () => {
  const { t } = useTranslation();
  const { lang, setLang, hideChangeLangModal, langModalShown } = useLang();
  const [selectedLang, setSelectedLang] = useState(lang);

  const chooseLang = () => {
    setLang(selectedLang);
    hideChangeLangModal();
  };

  return (
    <ClosableModal visible={langModalShown} close={hideChangeLangModal} animationType="fade">
      <View style={styles.content}>
        <View style={styles.title}>
          <Text>{t('general.select-lang')}</Text>
        </View>
        <LangList langs={langs} onSelect={setSelectedLang} selectedLang={selectedLang} />
      </View>
      <Button info block onPress={chooseLang}>
        <Text>{t('general.confirm')}</Text>
      </Button>
    </ClosableModal>
  );
};

export default ChangeLangModal;
