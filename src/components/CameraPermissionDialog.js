import React, { useState, useEffect } from 'react';
import { View, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { Card } from 'native-base';
import Permissions from 'react-native-permissions';
import { useAppContext } from '../contexts';
import CardItemText from './CardItemText';
import Navbar from './Navbar';
import PageWithButton from './PageWithButton';

const askCameraPermission = async () => {
  const permission = await Permissions.request('camera');
  return permission;
};

const doNothing = () => ({});

const CameraPermissionDialog = ({ reportPermission = doNothing }) => {
  const [cameraPermission, setCameraPermission] = useState(null);
  const { t } = useAppContext();

  const askPermission = async () => {
    const permission = await askCameraPermission();
    setCameraPermission(permission);
    reportPermission(permission === 'authorized');
  };

  useEffect(() => {
    askPermission();
  }, []);

  if (cameraPermission === 'denied')
    return (
      <>
        <Navbar title={t('camera.camera-permission')} hasClose />
        <PageWithButton
          buttonText="camera.grant-access"
          scrollEnabled={false}
          onButtonPress={askPermission}
          hideButton={Platform.OS === 'ios'}
        >
          <Card>
            <CardItemText text={t('camera.grant-access-guide')} />
            {Platform.OS === 'ios' && <CardItemText text={t('camera.no-permission-ios')} />}
          </Card>
        </PageWithButton>
      </>
    );

  return <View />;
};

CameraPermissionDialog.propTypes = {
  reportPermission: PropTypes.func,
};

CameraPermissionDialog.defaultProps = {
  reportPermission: doNothing,
};

export default CameraPermissionDialog;
