import React, { useState } from 'react';
import { Item } from 'native-base';
import PropTypes from 'prop-types';
import { Input } from './RtlAware';
import { useAppContext } from '../contexts';
import tools from '../utils/tools';
import { appParams } from '../constants';

const doNothing = () => ({});

const OtpInput = ({ onChange = doNothing, autoFocus = false }) => {
  const [success, setSuccess] = useState(false);
  const { t } = useAppContext();

  const reportOtp = text => {
    try {
      const normOtp = tools.string.normalizeOtp(text);
      setSuccess(true);
      onChange(normOtp);
    } catch (error) {
      setSuccess(false);
      onChange(null);
    }
  };

  return (
    <Item success={success}>
      <Input
        style={{ textAlign: 'center' }}
        placeholder={t('forms.otp-placeholder')}
        keyboardType="numeric"
        onChangeText={reportOtp}
        autoFocus={autoFocus}
        maxLength={appParams.otpMaxLen}
      />
    </Item>
  );
};

OtpInput.propTypes = {
  onChange: PropTypes.func,
  autoFocus: PropTypes.bool,
};

OtpInput.defaultProps = {
  onChange: doNothing,
  autoFocus: false,
};

export default OtpInput;
