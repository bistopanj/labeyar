/* eslint-disable react/prop-types */
import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { RNCamera } from 'react-native-camera';
import { Icon } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { useAppContext } from '../contexts';

import styles from '../styles/camera-toolbar-styles';

const { FlashMode: CameraFlashModes, Type: CameraTypes } = RNCamera.Constants;

const doNothing = () => ({});
const sideToolbarPosition = {
  ltr: { right: 0 },
  rtl: { left: 0 },
};

const bottomToolbarDirection = {
  ltr: { flexDirection: 'row' },
  rtl: { flexDirection: 'row-reverse' },
};

const FlashButton = ({ capturing = false, flashMode = CameraFlashModes.off, setFlashMode }) => {
  const toggleFlashMode = () =>
    setFlashMode(flashMode === CameraFlashModes.on ? CameraFlashModes.off : CameraFlashModes.on);
  return (
    <TouchableOpacity disabled={capturing} onPress={toggleFlashMode}>
      <Icon
        type="MaterialIcons"
        name={flashMode === CameraFlashModes.on ? 'flash-on' : 'flash-off'}
        style={styles.sideBtn}
      />
    </TouchableOpacity>
  );
};

FlashButton.propTypes = {
  capturing: PropTypes.bool,
  flashMode: PropTypes.number,
  setFlashMode: PropTypes.func,
};

FlashButton.defaultProps = {
  capturing: false,
  flashMode: CameraFlashModes.off,
  setFlashMode: doNothing,
};

const CaptureButton = ({ capturing = false, onCapture }) => (
  <TouchableOpacity onPress={onCapture} disabled={capturing}>
    <View style={[styles.captureBtn, capturing && styles.captureBtnActive]} />
  </TouchableOpacity>
);

CaptureButton.propTypes = {
  capturing: PropTypes.bool,
  onCapture: PropTypes.func,
};

CaptureButton.defaultProps = {
  capturing: false,
  onCapture: doNothing,
};

const ToggleCameraTypeButton = ({
  capturing = false,
  cameraType = CameraTypes.back,
  setCameraType,
}) => {
  const toggleCameraType = () =>
    setCameraType(cameraType === CameraTypes.back ? CameraTypes.front : CameraTypes.back);
  return (
    <TouchableOpacity disabled={capturing} onPress={toggleCameraType}>
      <Icon type="Ionicons" name="md-reverse-camera" style={styles.sideBtn} />
    </TouchableOpacity>
  );
};

ToggleCameraTypeButton.propTypes = {
  capturing: PropTypes.bool,
  cameraType: PropTypes.number,
  setCameraType: PropTypes.func,
};

ToggleCameraTypeButton.defaultProps = {
  capturing: false,
  cameraType: CameraTypes.back,
  setCameraType: doNothing,
};

const CameraToolbar = ({
  capturing = false,
  cameraType = CameraTypes.back,
  flashMode = CameraFlashModes.off,
  setFlashMode,
  setCameraType,
  onCapture,
}) => {
  const { orientation, direction } = useAppContext();
  if (orientation === 'portrait')
    return (
      <Grid style={styles.bottomToolbar}>
        <Row style={bottomToolbarDirection[direction]}>
          <Col style={styles.alignCenter}>
            <FlashButton capturing={capturing} flashMode={flashMode} setFlashMode={setFlashMode} />
          </Col>
          <Col size={2} style={styles.alignCenter}>
            <CaptureButton capturing={capturing} onCapture={onCapture} />
          </Col>
          <Col style={styles.alignCenter}>
            <ToggleCameraTypeButton
              capturing={capturing}
              setCameraType={setCameraType}
              cameraType={cameraType}
            />
          </Col>
        </Row>
      </Grid>
    );

  return (
    <Grid style={[styles.sideToolbar, sideToolbarPosition[direction]]}>
      <Col>
        <Row style={styles.alignCenter}>
          <FlashButton capturing={capturing} flashMode={flashMode} setFlashMode={setFlashMode} />
        </Row>
        <Row size={2} style={styles.alignCenter}>
          <CaptureButton capturing={capturing} onCapture={onCapture} />
        </Row>
        <Row style={styles.alignCenter}>
          <ToggleCameraTypeButton
            capturing={capturing}
            setCameraType={setCameraType}
            cameraType={cameraType}
          />
        </Row>
      </Col>
    </Grid>
  );
};

CameraToolbar.propTypes = {
  capturing: PropTypes.bool,
  cameraType: PropTypes.number,
  flashMode: PropTypes.number,
  setFlashMode: PropTypes.func,
  setCameraType: PropTypes.func,
  onCapture: PropTypes.func,
};

CameraToolbar.defaultProps = {
  capturing: false,
  cameraType: CameraTypes.back,
  flashMode: CameraFlashModes.off,
  setCameraType: doNothing,
  setFlashMode: doNothing,
  onCapture: doNothing,
};

export default CameraToolbar;
