import React, { useState } from 'react';
import { Item, Icon } from 'native-base';
import PropTypes from 'prop-types';
import { Input } from './RtlAware';
import { useAppContext } from '../contexts';
import tools from '../utils/tools';
import { appParams } from '../constants';

const doNothing = () => ({});

const MobileNumberInput = ({ onChange = doNothing, autoFocus = false }) => {
  const [success, setSuccess] = useState(false);
  const { t } = useAppContext();

  const updateMobileNumber = text => {
    try {
      const normNumber = tools.string.normalizePhoneNumber(text);
      setSuccess(true);
      onChange(normNumber);
    } catch (error) {
      setSuccess(false);
      onChange(null);
    }
  };

  return (
    <Item success={success}>
      <Icon active type="Entypo" name="old-mobile" />
      <Input
        style={{ textAlign: 'center' }}
        placeholder={t('forms.mobile-number-placeholder')}
        keyboardType="phone-pad"
        onChangeText={updateMobileNumber}
        autoFocus={autoFocus}
        maxLength={appParams.phoneNumberMaxLen}
      />
    </Item>
  );
};

MobileNumberInput.propTypes = {
  onChange: PropTypes.func,
  autoFocus: PropTypes.bool,
};

MobileNumberInput.defaultProps = {
  onChange: doNothing,
  autoFocus: false,
};

export default MobileNumberInput;
