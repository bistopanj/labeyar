import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Text } from './RtlAware';
import styles from '../styles/styles';
import { useLang } from '../contexts';

const selectorTitle = 'lang | زبان | لغة';

const LangSelector = () => {
  const { showChangeLangModal } = useLang();
  return (
    <TouchableOpacity onPress={showChangeLangModal}>
      <Text style={styles.secondaryText}>{`${selectorTitle} ▾`}</Text>
    </TouchableOpacity>
  );
};

export default LangSelector;
