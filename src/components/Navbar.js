import React from 'react';
import PropTypes from 'prop-types';
import { Header, Body, Left, Right, Button, Icon } from 'native-base';

import { navigate, goBack } from '../services/nav.service';

import TopMenu from './TopMenu';
import { Title, Subtitle } from './RtlAware';
import { appTexts } from '../constants';
import { useAppContext } from '../contexts';

const CancelButton = ({ hasClose = false, hasBack = false }) => {
  const { direction } = useAppContext();

  if (hasClose)
    return (
      <Left style={{ paddingHorizontal: 10 }}>
        <Button transparent onPress={() => navigate('Home')}>
          <Icon name="close" active />
        </Button>
      </Left>
    );

  if (hasBack)
    return (
      <Left style={{ paddingHorizontal: 10 }}>
        <Button transparent onPress={goBack}>
          <Icon name={direction === 'ltr' ? 'arrow-back' : 'arrow-forward'} active />
        </Button>
      </Left>
    );

  return null;
};

CancelButton.propTypes = {
  hasClose: PropTypes.bool,
  hasBack: PropTypes.bool,
};

CancelButton.defaultProps = {
  hasClose: false,
  hasBack: false,
};

const Navbar = ({
  title = appTexts.appName,
  subtitle = null,
  noMenu = false,
  hasClose = false,
  hasBack = false,
}) => {
  return (
    <Header>
      <CancelButton hasClose={hasClose} hasBack={hasBack} />
      <Body>
        <Title>{title}</Title>
        {subtitle && <Subtitle>{subtitle}</Subtitle>}
      </Body>
      {!noMenu && (
        <Right>
          <TopMenu />
        </Right>
      )}
    </Header>
  );
};

/*
<Header>
      <Left style={{ paddingHorizontal: 10 }}>
        {hasClose && (
          <Button transparent onPress={() => navigate('Home')}>
            <Icon name="close" active />
            <Icon name="arrow-forward" active />
          </Button>
        )}
      </Left>
      <Body>
        <Title>{title}</Title>
        {subtitle && <Subtitle>{subtitle}</Subtitle>}
      </Body>
      <Right>{!noMenu && <TopMenu />}</Right>
    </Header>
*/

Navbar.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  noMenu: PropTypes.bool,
  hasClose: PropTypes.bool,
  hasBack: PropTypes.bool,
};

Navbar.defaultProps = {
  title: '',
  subtitle: null,
  noMenu: false,
  hasClose: false,
  hasBack: false,
};

export default Navbar;
