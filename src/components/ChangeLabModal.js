import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { Button, ListItem, Left, Right, Radio, Spinner } from 'native-base';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import ClosableModal from './ClosableModal';
import { Text } from './RtlAware';
import { useAuth } from '../contexts/auth';
import { useToast } from '../contexts/toast';
import api from '../services/api.service';
import { translateForUser, errorTypes } from '../errors';

const styles = StyleSheet.create({
  title: {
    paddingBottom: 12,
  },
  content: {
    flex: 1,
  },
  labsContainer: {
    marginBottom: 12,
  },
  spinnerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const doNothing = () => ({});

const LabItem = ({ lab, onSelect, currentLab }) => {
  const { _id: labId } = lab;
  const { _id: currentLabId } = currentLab;
  return (
    <ListItem
      onPress={() => {
        onSelect(lab);
      }}
    >
      <Left>
        <Text>{lab.name}</Text>
      </Left>
      <Right>
        <Radio selected={labId === currentLabId} onPress={() => onSelect(lab)} />
      </Right>
    </ListItem>
  );
};

LabItem.propTypes = {
  lab: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  onSelect: PropTypes.func,
  currentLab: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
};

LabItem.defaultProps = {
  onSelect: doNothing,
};

const LabList = ({ labs = [], onSelect = doNothing, currentLab = {} }) => {
  return (
    <ScrollView style={styles.labsContainer}>
      {labs.map(lab => {
        const { _id: labId } = lab;
        return (
          <LabItem
            key={`lab-item-${labId}`}
            lab={lab}
            onSelect={onSelect}
            currentLab={currentLab}
          />
        );
      })}
    </ScrollView>
  );
};

LabList.propTypes = {
  labs: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ),
  onSelect: PropTypes.func,
  currentLab: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
};

LabList.defaultProps = {
  labs: [],
  onSelect: doNothing,
};

const ChangeLabModal = () => {
  const [isProcessing, setIsProcessing] = useState(false);
  const { userInfo, hideChangeLabModal, labsModalShown, setPrimaryLab } = useAuth();
  const { showError } = useToast();
  const { t } = useTranslation();
  const { labs = [], primaryLab = {} } = userInfo || {};
  const abortController = new AbortController();

  useEffect(() => {
    return () => {
      abortController.abort();
    };
  }, []);

  const changePrimaryLab = async lab => {
    try {
      const { _id: labId } = lab;
      setIsProcessing(true);
      await api.setUserPrimaryLab({ userInfo, primaryLab: labId }, abortController.signal);
      setPrimaryLab(lab);
      setIsProcessing(false);
      hideChangeLabModal();
    } catch (error) {
      if (error.name !== errorTypes.AbortError) showError(t(translateForUser(error)));
      setIsProcessing(false);
    }
  };

  if (labs.length > 0)
    return (
      <ClosableModal visible={labsModalShown} close={hideChangeLabModal} animationType="fade">
        {isProcessing ? (
          <View style={styles.spinnerContainer}>
            <Spinner />
          </View>
        ) : (
          <>
            <View style={styles.content}>
              <View style={styles.title}>
                <Text>{t('general.select-lab')}</Text>
              </View>
              <LabList labs={labs} onSelect={changePrimaryLab} currentLab={primaryLab} />
            </View>
            <Button info block onPress={hideChangeLabModal}>
              <Text>{t('general.confirm')}</Text>
            </Button>
          </>
        )}
      </ClosableModal>
    );
  return null;
};

export default ChangeLabModal;
