import React, { useRef } from 'react';
import { Icon, Button } from 'native-base';
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import { Text } from './RtlAware';
import { useAppContext } from '../contexts';
import { navigate } from '../services/nav.service';
import { translateForUser } from '../errors';

const triggerStyles = {
  triggerText: {},
  triggerOuterWrapper: { flex: 1 },
  triggerWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  triggerTouchable: {
    activeOpacity: 70,
    style: { flex: 1 },
  },
};

const optionsStyles = {
  optionsContainer: { padding: 5 },
  optionsWrapper: {},
  optionWrapper: {
    margin: 5,
    minHeight: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  optionTouchable: {
    underlayColor: '#f5b642',
    activeOpacity: 70,
  },
  optionText: {
    textAlign: 'center',
  },
};

const doNothing = () => ({});

const TopMenu = () => {
  const {
    t,
    logout = doNothing,
    showChangeLangModal = doNothing,
    showError,
    userInfo = {},
    showChangeLabModal = doNothing,
    showUpdateDialog = doNothing,
  } = useAppContext();
  const { labs = [] } = userInfo || {};
  const menu = useRef(null);

  const menuItems = [
    { value: 0, text: 'top-menu.home-page', action: () => navigate('Home') },
    { value: 1, text: 'top-menu.update-app', action: showUpdateDialog },
    { value: 2, text: 'top-menu.change-lang', action: showChangeLangModal },
    { value: 3, text: 'top-menu.change-lab', action: showChangeLabModal, hide: labs.length === 0 },
    { value: 4, text: 'top-menu.logout', action: logout },
  ];

  const openMenu = () => menu.current.open();

  const handleActions = value => {
    try {
      menuItems[value].action();
    } catch (error) {
      showError(t(translateForUser(error)));
    }
  };

  return (
    <Menu ref={menu} onSelect={handleActions}>
      <MenuTrigger customStyles={triggerStyles}>
        <Button transparent onPress={openMenu}>
          <Icon name="more" />
        </Button>
      </MenuTrigger>
      <MenuOptions customStyles={optionsStyles}>
        {menuItems.map(
          ({ value, text, hide = false }) =>
            !hide && (
              <MenuOption key={text} value={value}>
                <Text>{t(text)}</Text>
              </MenuOption>
            ),
        )}
      </MenuOptions>
    </Menu>
  );
};

export default TopMenu;
