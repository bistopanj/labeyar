import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const FullPageContainer = ({ children = null }) => {
  return <View style={styles.container}>{children}</View>;
};

FullPageContainer.propTypes = {
  children: PropTypes.node,
};

FullPageContainer.defaultProps = {
  children: null,
};

export default FullPageContainer;
