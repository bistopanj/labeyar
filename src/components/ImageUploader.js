import React, { Component } from 'react';
import { Image, View, StyleSheet } from 'react-native';
import { Icon } from 'native-base';
import PropTypes from 'prop-types';
import * as Progress from 'react-native-progress';
import tools from '../utils/tools';
import { errorTypes } from '../constants';
import { UnknownError } from '../errors';
import { AppContext } from '../contexts';

const componentStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {},
  overlay: {
    position: 'absolute',
    opacity: 1,
  },
  statusContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  statusIcon: {
    fontSize: 24,
  },
});

const StatusIndicator = ({ success = false, error = false }) => {
  if (success)
    return (
      <View style={componentStyles.statusContainer}>
        <Icon
          type="FontAwesome5"
          name="check"
          style={[componentStyles.statusIcon, { color: '#28a745' }]}
        />
      </View>
    );
  if (error)
    return (
      <View style={componentStyles.statusContainer}>
        <Icon
          type="FontAwesome5"
          name="exclamation"
          style={[componentStyles.statusIcon, { color: '#dc3545' }]}
        />
      </View>
    );
  return null;
};

StatusIndicator.propTypes = {
  success: PropTypes.bool,
  error: PropTypes.bool,
};

StatusIndicator.defaultProps = {
  success: false,
  error: false,
};

class ImageUploader extends Component {
  constructor(props) {
    super(props);
    this.abortController = new AbortController();
    this.state = {
      uploading: false,
      progress: 0,
      error: false,
      success: false,
    };
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  // This method updates the upload progress state by receiving the progress from xhr request.
  updateUploadProgress = evt => {
    if (evt.lengthComputable) {
      const percentComplete = evt.loaded / evt.total;
      this.setState({ progress: percentComplete });
    }
  };

  resetProgress = () => this.setState({ progress: 0 });

  upload = async () => {
    try {
      const { userInfo } = this.context;
      const {
        photo: { uri },
      } = this.props;
      this.setState({ uploading: true, success: false, error: false });
      const pubUrl = await tools.photo.upload(
        {
          uri,
          progressCb: this.updateUploadProgress,
          userInfo,
        },
        this.abortController.signal,
      );
      this.setState({ uploading: false, success: true });
      return pubUrl;
    } catch (error) {
      this.setState({ uploading: false, error: true });
      if (Object.keys(errorTypes).includes(error.type)) throw error;
      else {
        throw new UnknownError({
          code: 9999,
          message: error.message || 'something unknown caused problem in upload.ImageUploader',
        });
      }
    }
  };

  render() {
    const { uploading, progress, success, error } = this.state;
    const {
      style,
      photo: { uri, publicUrl },
    } = this.props;
    return (
      <View style={componentStyles.container}>
        <Image
          style={[componentStyles.image, style, { opacity: uploading ? 0.5 : 1 }]}
          source={{ uri }}
        />
        <StatusIndicator success={typeof publicUrl === 'string' || success} error={error} />
        {uploading ? (
          <View style={componentStyles.overlay}>
            <Progress.Pie progress={progress} size={50} color="white" borderWidth={0} />
          </View>
        ) : null}
      </View>
    );
  }
}

ImageUploader.contextType = AppContext;

ImageUploader.propTypes = {
  photo: PropTypes.shape({
    uri: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    publicUrl: PropTypes.string,
  }).isRequired,
  style: PropTypes.shape({}),
};

ImageUploader.defaultProps = {
  style: {},
};

export default ImageUploader;
