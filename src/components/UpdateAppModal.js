import React from 'react';
import { View, StyleSheet, Platform, Linking } from 'react-native';
import { Button, Spinner } from 'native-base';
import PropTypes from 'prop-types';
import ClosableModal from './ClosableModal';
import { Text, H3 } from './RtlAware';
import { useLang } from '../contexts/lang';
import { useUpdate } from '../contexts/update';

const styles = StyleSheet.create({
  title: {
    paddingBottom: 24,
  },
  content: {
    paddingBottom: 36,
  },
  spinnerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const doNothing = () => ({});

const WaitingMessage = ({ title = 'waiting' }) => (
  <>
    <View style={styles.title}>
      <Text>{title}</Text>
    </View>
    <View style={styles.spinnerContainer}>
      <Spinner />
    </View>
  </>
);

WaitingMessage.propTypes = {
  title: PropTypes.string,
};

WaitingMessage.defaultProps = {
  title: 'waiting',
};

const UpdateMessage = ({ isAvailable, updateInfo, close = doNothing }) => {
  const { t } = useLang();
  const description = isAvailable ? t('update.available') : t('update.not-available');
  const buttonText = isAvailable ? t('top-menu.update-app') : t('general.return');

  const goToWebAddress = () => {
    const { ios, android } = updateInfo || {};
    const appUpdateInfo = Platform.OS === 'ios' ? ios : android;
    Linking.openURL(appUpdateInfo.link);
  };

  const opButtonPress = isAvailable ? goToWebAddress : close;

  return (
    <>
      <View>
        <View style={styles.title}>
          <H3>{t('top-menu.update-app')}</H3>
        </View>
        <View style={styles.content}>
          <Text>{description}</Text>
        </View>
      </View>
      <Button info block onPress={opButtonPress}>
        <Text>{buttonText}</Text>
      </Button>
    </>
  );
};

UpdateMessage.propTypes = {
  isAvailable: PropTypes.bool.isRequired,
  updateInfo: PropTypes.shape({
    version: PropTypes.string,
    ios: PropTypes.shape({
      verion: PropTypes.string,
      link: PropTypes.string,
    }),
    android: PropTypes.shape({
      verion: PropTypes.string,
      link: PropTypes.string,
    }),
  }),
  close: PropTypes.func,
};

UpdateMessage.defaultProps = {
  close: doNothing,
  updateInfo: {},
};

const UpdateAppModal = () => {
  const {
    updateDialogShown,
    hideUpdateDialog,
    isUpdateAvailable,
    updateInfo,
    lookingForUpdate,
  } = useUpdate();
  const { t } = useLang();
  return (
    <ClosableModal
      visible={updateDialogShown}
      close={hideUpdateDialog}
      animationType="fade"
      boxStyle={{ minHeight: 0 }}
    >
      {lookingForUpdate ? (
        <WaitingMessage title={t('update.looking-for-update-info')} />
      ) : (
        <UpdateMessage
          isAvailable={isUpdateAvailable}
          updateInfo={updateInfo}
          close={hideUpdateDialog}
        />
      )}
    </ClosableModal>
  );
};

export default UpdateAppModal;
