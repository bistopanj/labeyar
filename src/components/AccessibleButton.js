/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { View, Dimensions } from 'react-native';
import { Spinner, Button } from 'native-base';
import PropTypes from 'prop-types';
import { Text } from './RtlAware';
import { appParams } from '../constants';
import { useAppContext } from '../contexts';

const { width: winWidth } = Dimensions.get('window');

const actionCenterStyle = {
  portrait: {
    height: appParams.actionCenterHeight,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  landscape: {
    width: appParams.actionCenterWidthRatio * winWidth,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
  },
};

const AccessibleButton = props => {
  const { orientation } = useAppContext();
  const { containerStyle = {}, text = 'Confirm', isProcessing = false } = props;
  return (
    <View style={[actionCenterStyle[orientation], containerStyle]}>
      <Button {...props} block transparent={isProcessing}>
        {!isProcessing ? <Text>{text}</Text> : <Spinner />}
      </Button>
    </View>
  );
};

AccessibleButton.propTypes = {
  containerStyle: PropTypes.shape({}),
  text: PropTypes.string,
  isProcessing: PropTypes.bool,
};

AccessibleButton.defaultProps = {
  containerStyle: {},
  text: 'Confirm',
  isProcessing: false,
};

export default AccessibleButton;
