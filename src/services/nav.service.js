import { NavigationActions } from 'react-navigation';

let navigator;

const setTopLevelNavigator = ref => {
  navigator = ref;
};

const navigate = (routeName, params) => {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );
};

const goBack = () => {
  navigator.dispatch(NavigationActions.back({ key: null }));
};

export { navigate, setTopLevelNavigator, goBack };
