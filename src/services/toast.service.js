import { Toast } from 'native-base';

const defaultValues = {
  text: '',
  duration: 5000,
  type: null,
  position: 'bottom',
  textStyle: { textAlign: 'center' },
  buttonText: null,
  buttonTextStyle: {},
};

const showToast = ({
  text = defaultValues.text,
  duration = defaultValues.duration,
  type = defaultValues.type,
  position = defaultValues.position,
  textStyle = defaultValues.textStyle,
  buttonText = defaultValues.buttonText,
  buttonTextStyle = defaultValues.buttonStyle,
}) =>
  Toast.show({
    text,
    duration,
    type,
    position,
    textStyle,
    buttonText,
    buttonTextStyle,
  });

const showError = text =>
  Toast.show({
    text,
    duration: defaultValues.duration,
    type: 'danger',
    position: defaultValues.position,
    textStyle: defaultValues.textStyle,
    buttonText: defaultValues.buttonText,
    buttonTextStyle: defaultValues.buttonStyle,
  });

const showSuccess = text =>
  Toast.show({
    text,
    duration: defaultValues.duration,
    type: 'success',
    position: defaultValues.position,
    textStyle: defaultValues.textStyle,
    buttonText: defaultValues.buttonText,
    buttonTextStyle: defaultValues.buttonStyle,
  });

const showInfo = text =>
  Toast.show({
    text,
    duration: defaultValues.duration,
    position: defaultValues.position,
    textStyle: defaultValues.textStyle,
    buttonText: defaultValues.buttonText,
    buttonTextStyle: defaultValues.buttonStyle,
  });

export { Toast, showToast, showError, showSuccess, showInfo };
