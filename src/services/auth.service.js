import AsyncStorage from '@react-native-community/async-storage';
import jwtDecode from 'jwt-decode';
import { localStorageKeys, errorTypes } from '../constants';
import api from './api.service';
import { UnknownError, DeviceError } from '../errors';

const { userInfo: userInfoKey } = localStorageKeys;

const findAllUserLabs = ({ labsOwned, userInfo }) => {
  const { labs, primaryLab } = userInfo;
  const userOwnedLabs = labsOwned.map(l => ({ ...l, owner: true }));
  const otherLabs = labs.map(l => ({ ...l, owner: false }));
  const allLabs = [...userOwnedLabs, ...otherLabs];
  return { allLabs, primaryLab };
};

const getOtp = async (mobileNumber, signal = null) => {
  try {
    const otpData = await api.getOtp(mobileNumber, signal);
    return otpData;
  } catch (error) {
    if (Object.keys(errorTypes).includes(error.type)) throw error;
    else {
      throw new UnknownError({
        code: 9999,
        message: error.message || 'something unknown caused problem in getOtp.auth.service',
      });
    }
  }
};

const saveOnDevice = obj => AsyncStorage.setItem(userInfoKey, JSON.stringify(obj));

const login = async ({ mobileNumber, otp }, signal = null) => {
  try {
    const userInfo = await api.login({ mobileNumber, otp }, signal);
    // const labsOwned = await api.getUserLabs({ userInfo }, signal);
    // const { allLabs, primaryLab } = findAllUserLabs({ labsOwned, userInfo });
    const { token, primaryLab } = userInfo;
    const tokenPayload = jwtDecode(token);
    const { user } = tokenPayload;
    const fullUserInfo = { ...userInfo, user, primaryLab };
    await saveOnDevice(fullUserInfo);
    return fullUserInfo;
  } catch (error) {
    if (Object.keys(errorTypes).includes(error.type)) throw error;
    else {
      throw new UnknownError({
        code: 9999,
        message: error.message || 'something unknown caused problem in login.auth.service',
      });
    }
  }
};

const logout = async () => {
  try {
    await AsyncStorage.removeItem(userInfoKey);
  } catch (error) {
    throw new DeviceError({
      code: 1003,
      message:
        error.message || 'Error happened when trying to remove user info from device storage',
    });
  }
};

const isTokenValid = token => {
  try {
    const tokenPayload = jwtDecode(token);
    const { exp } = tokenPayload;
    const tokenExpDate = new Date(exp * 1000);
    const nowDate = new Date();
    return tokenExpDate.getTime() > nowDate.getTime();
  } catch (error) {
    return false;
  }
};

const checkSignIn = async () => {
  try {
    const userInfoStr = await AsyncStorage.getItem(userInfoKey);
    const userInfo = JSON.parse(userInfoStr);
    const { token } = userInfo || {};
    if (isTokenValid(token)) return userInfo;
    return false;
  } catch (error) {
    throw new DeviceError({
      code: 1002,
      message:
        error.message || 'Error happened when trying to retrieve user info from device storage',
    });
  }
};

const updateUserInfo = async ({ userInfo }, signal = null) => {
  try {
    const uInfo = await api.getUserInfo({ userInfo }, signal);
    const labsOwned = await api.getUserLabs({ userInfo }, signal);
    const { allLabs, primaryLab } = findAllUserLabs({ labsOwned, userInfo: uInfo });
    const { token, user } = userInfo;
    const newUserInfo = { token, primaryLab, user, labs: allLabs };
    return newUserInfo;
  } catch (error) {
    if (Object.keys(errorTypes).includes(error.type)) throw error;
    else {
      throw new UnknownError({
        code: 9999,
        message: error.message || 'something unknown caused problem in updateUserInfo.auth.service',
      });
    }
  }
};

export { checkSignIn, getOtp, login, logout, updateUserInfo };
