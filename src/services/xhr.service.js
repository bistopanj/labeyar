import { ServerError, DeviceError, AbortError } from '../errors';

const DONE_STATE = 4;
const NO_INTERNET_STATE = 0;

function buildError(req) {
  if (req.readyState === NO_INTERNET_STATE)
    return new DeviceError({ code: 1006, message: 'Problem connecting to upload server.' });

  if (req.status === 0)
    return new ServerError({ code: 9996, message: 'something is wrong with the url' });

  if (req.status === 404) return new ServerError({ code: 404, message: 'Not Found' });

  return new ServerError({
    status: req.status,
    code: 1005,
    message:
      ((req.response || {}).error || {}).message ||
      'something went wrong when trying to perform xhr operation.',
  });
}

function xhr(url, options = {}) {
  return new Promise((resolve, reject) => {
    if (options.signal && options.signal.aborted)
      return reject(
        new AbortError({ code: 9998, message: 'image download request aborted by user.' }),
      );

    const method = options.method || 'get';
    const request = new XMLHttpRequest();

    function abort() {
      request.abort();
    }

    request.open(method, url, true);

    request.responseType = options.responseType || 'json';

    if (options.onProgress) request.onprogress = options.onProgress;
    if (options.onUploadProgress) request.upload.onprogress = options.onUploadProgress;
    if (options.onStart) request.onloadstart = options.onStart;

    request.onload = () => {
      if (request.status >= 200 && request.status < 400) {
        return resolve(request.response);
      }

      return reject(buildError(request));
    };

    if (options.signal) {
      options.signal.addEventListener('abort', abort);
      request.onreadystatechange = () => {
        if (request.readyState === DONE_STATE) {
          options.signal.removeEventListener('abort', abort);
        }
      };
    }

    request.onabort = () => {
      reject(new AbortError({ code: 9998, message: 'request aborted by user.' }));
    };

    request.onerror = () => {
      reject(buildError(request));
    };
    request.ontimeout = () => {
      reject(buildError(request));
    };

    request.send(options.body || null);
  });
}

export default xhr;
