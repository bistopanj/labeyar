import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import ar from '../locales/ar/translation.json';
import en from '../locales/en/translation.json';
import fa from '../locales/fa/translation.json';
import fr from '../locales/fr/translation.json';

const resources = {
  ar: { translation: ar },
  en: { translation: en },
  fa: { translation: fa },
  fr: { translation: fr },
};

i18n.use(initReactI18next).init({
  fallbackLng: 'en',
  debug: false,
  initImmediate: false,
  preload: ['en', 'fa'],
  lng: 'en',
  resources,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
