/* eslint-disable no-undef */
import { appParams } from '../constants';
import { ServerError, DeviceError, errorTypes } from '../errors';

const getUploadPermission = async (userInfo, signal = null) => {
  try {
    const { token, primaryLab } = userInfo;
    const { _id: labId } = primaryLab;
    const url = `${appParams.serverBaseAddress}api/labs/${labId}/upload`;
    const response = await fetch(url, {
      method: 'GET',
      signal,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'getUploadPermission request aborted by user.',
      });
    } else {
      throw new DeviceError({
        code: 1004,
        message:
          error.message || 'Error happened when trying to get upload permission from server.',
      });
    }
  }
};

// This method sends the new result to server.
const addResult = async ({ result, userInfo }, signal = null) => {
  try {
    const { token, primaryLab } = userInfo;
    const { _id: labId } = primaryLab;
    const addResultUrl = `${appParams.serverBaseAddress}api/labs/${labId}/results`;
    const finalResult = { informClient: true, ...result };
    const response = await fetch(addResultUrl, {
      method: 'POST',
      signal,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(finalResult),
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson.data;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'addResult request aborted by user.',
      });
    } else {
      throw new DeviceError({
        code: 1007,
        message: error.message || 'Error happened when trying submit a new result to server.',
      });
    }
  }
};

const getOtp = async (mobileNumber, signal = null) => {
  const getOtpUrl = `${appParams.serverBaseAddress}auth/otp`;
  try {
    const response = await fetch(getOtpUrl, {
      method: 'POST',
      signal,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ mobileNumber }),
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson.data;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'getOtp request aborted by user.',
      });
    } else {
      throw new DeviceError({
        code: 1000,
        message: error.message || 'Error happened when trying to get otp from server.',
      });
    }
  }
};

const getUserLabs = async ({ userInfo }, signal = null) => {
  try {
    const { token } = userInfo;
    const url = `${appParams.serverBaseAddress}api/labs`;
    const response = await fetch(url, {
      method: 'GET',
      signal,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'getUserLabs request aborted by user.',
      });
    } else {
      throw new DeviceError({
        code: 1015,
        message: error.message || 'Error happened when trying to getUserLabs.',
      });
    }
  }
};

const setUserPrimaryLab = async ({ userInfo, primaryLab }, signal = null) => {
  try {
    const {
      token,
      user: { _id: userId },
    } = userInfo;
    const url = `${appParams.serverBaseAddress}api/users/${userId}/primaryLab`;
    const response = await fetch(url, {
      method: 'PUT',
      signal,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({ primaryLab }),
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'setUserPrimaryLab request aborted by user.',
      });
    } else {
      throw new DeviceError({
        code: 1016,
        message: error.message || 'Error happened when trying to setUserPrimaryLab.',
      });
    }
  }
};

const getUpdateInfo = async ({ userInfo }, signal = null) => {
  try {
    const { token } = userInfo;
    const url = `${appParams.serverBaseAddress}api/info/latest-app-version`;
    const response = await fetch(url, {
      method: 'GET',
      signal,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'getLatestVersionNumber request aborted by user.',
      });
    } else {
      throw new DeviceError({
        code: 1017,
        message: error.message || 'Error happened when trying to getLatestVersionNumber.',
      });
    }
  }
};

const getUserInfo = async ({ userInfo }, signal = null) => {
  try {
    const {
      token,
      user: { _id: userId },
    } = userInfo;
    const url = `${appParams.serverBaseAddress}api/users/${userId}?populate=labs`;
    const response = await fetch(url, {
      method: 'GET',
      signal,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'getUserInfo request aborted by user.',
      });
    } else {
      throw new DeviceError({
        code: 1015,
        message: error.message || 'Error happened when trying to getUserInfo.',
      });
    }
  }
};

const login = async ({ mobileNumber, otp }, signal = null) => {
  const loginpUrl = `${appParams.serverBaseAddress}auth/login`;
  try {
    const response = await fetch(loginpUrl, {
      method: 'POST',
      signal,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ mobileNumber, otp }),
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'login request aborted by user.',
      });
    } else {
      throw new DeviceError({
        code: 1001,
        message: error.message || 'Error happened when trying to login.',
      });
    }
  }
};

export default {
  getUploadPermission,
  addResult,
  getOtp,
  login,
  getUserInfo,
  getUserLabs,
  setUserPrimaryLab,
  getUpdateInfo,
};
