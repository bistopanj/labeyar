import { errorTypes, UnknownError } from '../errors';
import api from './api.service';

const addResult = async ({ result, userInfo }, signal = null) => {
  try {
    const theResult = await api.addResult({ result, userInfo }, signal);
    return theResult;
  } catch (error) {
    if (Object.keys(errorTypes).includes(error.type)) throw error;
    else
      throw new UnknownError({
        code: 9999,
        message: error.message || 'something unknown caused problem in addResult.result.service',
      });
  }
};

export { addResult };
