import { I18nManager } from 'react-native';
import RNRestart from 'react-native-restart';
import AsyncStorage from '@react-native-community/async-storage';
import * as RNLocalize from 'react-native-localize';
import { localStorageKeys, langs, fontFamilies, timing } from '../constants';
import { DeviceError } from '../errors';

const { appLang: appLangKey } = localStorageKeys;

const doNothing = () => ({});

const getLangInfo = lng => {
  if (lng in langs) return langs[lng];
  return langs.en;
};

const getLangDir = lng => {
  const lngInfo = getLangInfo(lng);
  return lngInfo.rtl ? 'rtl' : 'ltr';
};

const findFontFamily = (lng, type) => {
  if (lng in fontFamilies) return fontFamilies[lng][type];
  return fontFamilies.en[type];
};

const changeRTL = (isRTL, setTimer = doNothing) => {
  I18nManager.allowRTL(isRTL);
  I18nManager.forceRTL(isRTL);
  const timer = setTimeout(() => {
    RNRestart.Restart();
  }, timing.changeDirectionTimeout);

  setTimer(timer);
};

const findDefaultAppLang = async () => {
  try {
    const appLang = await AsyncStorage.getItem(appLangKey);
    if (appLang) return appLang;
    const bestLang = RNLocalize.findBestAvailableLanguage(Object.keys(langs));
    return bestLang.languageTag;
  } catch (error) {
    return 'en';
  }
};

const saveAppLang = async lng => {
  try {
    await AsyncStorage.setItem(appLangKey, lng);
  } catch (error) {
    throw new DeviceError({
      code: 1013,
      message:
        error.message || 'Error happened when trying to save selected language to device storage',
    });
  }
};

const ensureDirection = (dir, informCb, setTimer = doNothing) => {
  const shouldBeRTL = dir === 'rtl';
  const { isRTL: phoneRTL } = I18nManager;
  if (shouldBeRTL !== phoneRTL) {
    informCb();
    changeRTL(shouldBeRTL, setTimer);
  }
};

export {
  getLangInfo,
  getLangDir,
  changeRTL,
  findDefaultAppLang,
  saveAppLang,
  ensureDirection,
  findFontFamily,
};
