import React, { createContext, useContext, useState } from 'react';
import { Dimensions } from 'react-native';
import PropTypes from 'prop-types';

const { width: initialWinWidth, height: initialWinHeight } = Dimensions.get('window');
const initialOrientation = initialWinWidth > initialWinHeight ? 'landscape' : 'portrait';

const DisplayContext = createContext();
const DisplayConsumer = DisplayContext.Consumer;
const useDisplay = () => useContext(DisplayContext);

const DisplayProvider = ({ children }) => {
  const [width, setWidth] = useState(initialWinWidth);
  const [height, setHeight] = useState(initialWinHeight);
  const [orientation, setOrientation] = useState(initialOrientation);

  const updateDisplayInfo = e => {
    const { width: w, height: h } = e.nativeEvent.layout;
    const { width: winW, height: winH } = Dimensions.get('window');
    const orient = winW > winH ? 'landscape' : 'portrait';
    setWidth(w);
    setHeight(h);
    setOrientation(orient);
  };

  const contextValues = { width, height, orientation, updateDisplayInfo };
  return <DisplayContext.Provider value={contextValues}>{children}</DisplayContext.Provider>;
};

DisplayProvider.propTypes = {
  children: PropTypes.node,
};

DisplayProvider.defaultProps = {
  children: null,
};

export { DisplayContext, DisplayConsumer, DisplayProvider, useDisplay };
