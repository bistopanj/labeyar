import React, { createContext, useContext, useState } from 'react';
import { Platform } from 'react-native';
import PropTypes from 'prop-types';
import { appVersion } from '../constants';
import api from '../services/api.service';
import { useAuth } from './auth';
import { compareVersions } from '../utils/tools';

const UpdateContext = createContext();
const UpdateConsumer = UpdateContext.Consumer;
const useUpdate = () => useContext(UpdateContext);

const UpdateProvider = ({ children }) => {
  const [isUpdateAvailable, setIsUpdateAvailable] = useState(false);
  const [updateInfo, setUpdateInfo] = useState(null);
  const [lookingForUpdate, setLookingForUpdate] = useState(false);
  const [updateDialogShown, setUpdateDialogShown] = useState(false);
  const { userInfo } = useAuth();

  const abortController = new AbortController();

  const getUpdateInfo = async () => {
    setLookingForUpdate(true);
    const upInfo = await api.getUpdateInfo({ userInfo }, abortController.signal);
    setLookingForUpdate(false);
    setUpdateInfo(upInfo);
    return upInfo;
  };

  const checkForUpdate = async () => {
    const upInfo = await getUpdateInfo();
    const latestVersion = upInfo[Platform.OS === 'ios' ? 'ios' : 'android'].version;
    const isAvailable = compareVersions(appVersion, latestVersion, true);
    setIsUpdateAvailable(isAvailable);
  };

  const hideUpdateDialog = () => setUpdateDialogShown(false);
  const showUpdateDialog = async () => {
    setUpdateDialogShown(true);
    await checkForUpdate();
  };

  const contextValues = {
    showUpdateDialog,
    hideUpdateDialog,
    isUpdateAvailable,
    lookingForUpdate,
    updateDialogShown,
    appVersion,
    updateInfo,
  };

  return <UpdateContext.Provider value={contextValues}>{children}</UpdateContext.Provider>;
};

UpdateProvider.propTypes = {
  children: PropTypes.node,
};

UpdateProvider.defaultProps = {
  children: null,
};

export { UpdateContext, UpdateConsumer, UpdateProvider, useUpdate };
