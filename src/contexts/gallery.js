import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

const GalleryContext = createContext();
const GalleryConsumer = GalleryContext.Consumer;
const useGallery = () => useContext(GalleryContext);

const GalleryProvider = ({ children }) => {
  const [photos, setPhotos] = useState([]);
  const [count, setCount] = useState(0);
  const [allUploaded, setAllUploaded] = useState(false);

  const markAllUploaded = () => setAllUploaded(true);
  const clearAllUploaded = () => setAllUploaded(false);
  const clearGallery = () => {
    setPhotos([]);
    setAllUploaded(false);
  };

  const addPhoto = photo => {
    const id = `photo${count + 1}`;
    setCount(count + 1);
    setPhotos([...photos, { id, ...photo }]);
  };

  const removePhoto = photo => {
    const newPhotos = photos.filter(p => p.id !== photo.id);
    setPhotos(newPhotos);
  };

  const movePhotoForward = photo => {
    const index = photos.findIndex(p => p.id === photo.id);
    if (index >= 0 && index + 1 < photos.length) {
      const newPhotos = [...photos];
      [newPhotos[index], newPhotos[index + 1]] = [newPhotos[index + 1], newPhotos[index]];
      setPhotos(newPhotos);
    }
  };

  const movePhotoBack = photo => {
    const index = photos.findIndex(p => p.id === photo.id);
    if (index - 1 >= 0) {
      const newPhotos = [...photos];
      [newPhotos[index - 1], newPhotos[index]] = [newPhotos[index], newPhotos[index - 1]];
      setPhotos(newPhotos);
    }
  };

  const addPublicUrl = (photo, publicUrl) => {
    const index = photos.findIndex(p => p.id === photo.id);
    if (index > -1) {
      setPhotos(oldArray => {
        const newPhotos = [...oldArray];
        const updatedPhoto = { ...newPhotos[index], publicUrl };
        newPhotos[index] = updatedPhoto;
        return newPhotos;
      });
    }
  };

  const contextValues = {
    addPhoto,
    photos,
    removePhoto,
    movePhotoForward,
    movePhotoBack,
    addPublicUrl,
    allUploaded,
    markAllUploaded,
    clearAllUploaded,
    clearGallery,
  };
  return <GalleryContext.Provider value={contextValues}>{children}</GalleryContext.Provider>;
};

GalleryProvider.propTypes = {
  children: PropTypes.node,
};

GalleryProvider.defaultProps = {
  children: null,
};

export { GalleryContext, GalleryConsumer, GalleryProvider, useGallery };
