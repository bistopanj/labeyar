import React, { createContext, useContext } from 'react';
import PropTypes from 'prop-types';
import { Toast, showToast as _showToast } from '../services/toast.service';
import { findFontFamily, getLangDir } from '../services/lang.service';

const ToastContext = createContext();
const ToastConsumer = ToastContext.Consumer;
const useToast = () => useContext(ToastContext);

const ToastProvider = ({ children, lang = 'en' }) => {
  const showToast = ({ textStyle, ...rest }) => {
    const newTextStyle = {
      fontSize: 12,
      textAlign: 'justify',
      writingDirection: getLangDir(lang),
      fontFamily: findFontFamily(lang, 'p'),
    };
    _showToast({ textStyle: newTextStyle, ...rest });
  };
  const showError = text => showToast({ text, type: 'danger' });
  const showSuccess = text => showToast({ text, type: 'success' });
  const showInfo = text => showToast({ text });

  const contextValues = {
    showToast,
    showError,
    showSuccess,
    showInfo,
    Toast,
  };
  return <ToastContext.Provider value={contextValues}>{children}</ToastContext.Provider>;
};

ToastProvider.propTypes = {
  children: PropTypes.node,
  lang: PropTypes.string,
};

ToastProvider.defaultProps = {
  children: null,
  lang: 'en',
};

export { ToastContext, ToastConsumer, ToastProvider, useToast };
