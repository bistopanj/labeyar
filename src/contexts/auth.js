import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';
import {
  logout as _logout,
  checkSignIn,
  getOtp,
  login as _login,
  updateUserInfo as _updateUserInfo,
} from '../services/auth.service';
import { navigate } from '../services/nav.service';

const AuthContext = createContext();
const AuthConsumer = AuthContext.Consumer;
const useAuth = () => useContext(AuthContext);

const AuthProvider = ({ children }) => {
  const [userInfo, setUserInfo] = useState(null);
  const [labsModalShown, setLabsModalShown] = useState(false);

  const login = async ({ mobileNumber, otp }, signal) => {
    const uInfo = await _login({ mobileNumber, otp }, signal);
    setUserInfo(uInfo);
    return uInfo;
  };

  const loadUserInfo = async () => {
    const localUserInfo = await checkSignIn();
    if (localUserInfo) setUserInfo(localUserInfo);
  };

  const updateUserInfo = async (signal = null) => {
    const uInfo = await _updateUserInfo({ userInfo }, signal);
    setUserInfo(uInfo);
  };

  const logout = async () => {
    await _logout();
    navigate('Auth');
  };

  const setPrimaryLab = lab => {
    setUserInfo(oldUserInfo => ({ ...oldUserInfo, primaryLab: lab }));
  };

  const showChangeLabModal = () => setLabsModalShown(true);
  const hideChangeLabModal = () => setLabsModalShown(false);

  const contextValues = {
    userInfo,
    setUserInfo,
    logout,
    loadUserInfo,
    getOtp,
    login,
    updateUserInfo,
    setPrimaryLab,
    labsModalShown,
    showChangeLabModal,
    hideChangeLabModal,
  };
  return <AuthContext.Provider value={contextValues}>{children}</AuthContext.Provider>;
};

AuthProvider.propTypes = {
  children: PropTypes.node,
};

AuthProvider.defaultProps = {
  children: null,
};

export { AuthContext, AuthConsumer, useAuth, AuthProvider };
