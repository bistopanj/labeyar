import React, { createContext, useContext } from 'react';
import PropTypes from 'prop-types';
import { MenuProvider } from 'react-native-popup-menu';
import { useTranslation } from 'react-i18next';
import { Root, StyleProvider } from 'native-base';
import getTheme from '../../native-base-theme/components';
import labsoozTheme from '../../native-base-theme/variables/labsooz';
import { LangProvider, LangConsumer, LangContext, useLang } from './lang';
import { DisplayContext, DisplayConsumer, DisplayProvider, useDisplay } from './display';
import { AuthContext, AuthConsumer, useAuth, AuthProvider } from './auth';
import { ToastContext, ToastConsumer, ToastProvider, useToast } from './toast';
import { GalleryContext, GalleryConsumer, GalleryProvider, useGallery } from './gallery';
import { ResultContext, ResultConsumer, ResultProvider, useResult } from './result';
import { UpdateContext, UpdateConsumer, UpdateProvider, useUpdate } from './update';
import ChangeLabModal from '../components/ChangeLabModal';
import ChangeLangModal from '../components/ChangeLangModal';
import UpdateAppModal from '../components/UpdateAppModal';

const AppContext = createContext();
const AppContextConsumer = AppContext.Consumer;
const useAppContext = () => useContext(AppContext);

const AppContextProvider = ({ children, defaultLang = 'en' }) => {
  return (
    <DisplayProvider>
      <DisplayConsumer>
        {displayContext => (
          <Root onLayout={displayContext.updateDisplayInfo}>
            <LangProvider defaultLang={defaultLang}>
              <AuthProvider>
                <AuthConsumer>
                  {authContext => (
                    <MenuProvider>
                      <StyleProvider style={getTheme(labsoozTheme)}>
                        <LangConsumer>
                          {langContext => (
                            <ToastProvider lang={langContext.lang}>
                              <ToastConsumer>
                                {toastContext => (
                                  <GalleryProvider>
                                    <GalleryConsumer>
                                      {galleryContext => (
                                        <ResultProvider>
                                          <ResultConsumer>
                                            {resultContext => (
                                              <UpdateProvider>
                                                <UpdateConsumer>
                                                  {updateContext => (
                                                    <AppContext.Provider
                                                      value={{
                                                        ...displayContext,
                                                        ...langContext,
                                                        ...authContext,
                                                        ...toastContext,
                                                        ...galleryContext,
                                                        ...resultContext,
                                                        ...updateContext,
                                                      }}
                                                    >
                                                      {children}
                                                      <ChangeLabModal />
                                                      <ChangeLangModal />
                                                      <UpdateAppModal />
                                                    </AppContext.Provider>
                                                  )}
                                                </UpdateConsumer>
                                              </UpdateProvider>
                                            )}
                                          </ResultConsumer>
                                        </ResultProvider>
                                      )}
                                    </GalleryConsumer>
                                  </GalleryProvider>
                                )}
                              </ToastConsumer>
                            </ToastProvider>
                          )}
                        </LangConsumer>
                      </StyleProvider>
                    </MenuProvider>
                  )}
                </AuthConsumer>
              </AuthProvider>
            </LangProvider>
          </Root>
        )}
      </DisplayConsumer>
    </DisplayProvider>
  );
};

AppContextProvider.propTypes = {
  children: PropTypes.node,
  defaultLang: PropTypes.string,
};

AppContextProvider.defaultProps = {
  children: null,
  defaultLang: 'en',
};

export default AppContext;
export {
  AppContext,
  AppContextConsumer,
  useAppContext,
  AppContextProvider,
  LangProvider,
  LangConsumer,
  LangContext,
  useLang,
  DisplayContext,
  DisplayConsumer,
  DisplayProvider,
  useDisplay,
  AuthContext,
  AuthConsumer,
  useAuth,
  AuthProvider,
  ToastContext,
  ToastConsumer,
  ToastProvider,
  useToast,
  GalleryContext,
  GalleryConsumer,
  GalleryProvider,
  useGallery,
  ResultContext,
  ResultConsumer,
  ResultProvider,
  useResult,
  UpdateContext,
  UpdateConsumer,
  UpdateProvider,
  useUpdate,
};
