import React, { createContext, useEffect, useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { showInfo, showError } from '../services/toast.service';
import { getLangInfo, saveAppLang, ensureDirection } from '../services/lang.service';
import { translateForUser } from '../errors';
import { navigate } from '../services/nav.service';

const LangContext = createContext();
const LangConsumer = LangContext.Consumer;
const useLang = () => useContext(LangContext);
const doNothing = () => ({});

const LangProvider = ({ children, defaultLang = 'en' }) => {
  const [direction, setDirection] = useState('ltr');
  const [lang, setLang] = useState(defaultLang);
  const [langModalShown, setLangModalShown] = useState(false);
  const [langInfo, setLangInfo] = useState(getLangInfo(defaultLang));
  const { t, i18n } = useTranslation();

  const abortController = new AbortController();

  const hideChangeLangModal = () => setLangModalShown(false);
  const showChangeLangModal = () => setLangModalShown(true);

  const informRestartToUser = () => {
    showInfo(t('general.restarting-for-lang'));
    navigate('Waiting');
  };

  const changeLang = async (lng, setTimer = doNothing) => {
    try {
      const lngInfo = getLangInfo(lng);
      const dir = lngInfo.rtl ? 'rtl' : 'ltr';
      i18n.changeLanguage(lng);
      setLangInfo(lngInfo);
      setDirection(dir);
      await saveAppLang(lng);
      ensureDirection(dir, informRestartToUser, setTimer);
    } catch (error) {
      showError(t(translateForUser(error)));
    }
  };

  useEffect(() => {
    return () => {
      abortController.abort();
    };
  }, []);

  useEffect(() => {
    let changeDirTimer;
    const setTimer = timer => {
      changeDirTimer = timer;
    };
    changeLang(lang, setTimer);
    return () => {
      clearTimeout(changeDirTimer);
    };
  }, [lang]);

  const contextValues = {
    direction,
    lang,
    langInfo,
    langModalShown,
    showChangeLangModal,
    hideChangeLangModal,
    setLang,
    t,
    i18n,
  };

  return <LangContext.Provider value={contextValues}>{children}</LangContext.Provider>;
};

LangProvider.propTypes = {
  children: PropTypes.node,
  defaultLang: PropTypes.string,
};

LangProvider.defaultProps = {
  children: null,
  defaultLang: 'en',
};

export { LangProvider, LangConsumer, LangContext, useLang };
