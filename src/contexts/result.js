import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { useGallery } from './gallery';
import { useAuth } from './auth';
import { ValidationError } from '../errors';
import api from '../services/api.service';

const ResultContext = createContext();
const ResultConsumer = ResultContext.Consumer;
const useResult = () => useContext(ResultContext);

const ResultProvider = ({ children }) => {
  const [clientMobileNumber, setClientMobileNumber] = useState(null);
  const [result, setResult] = useState(null);
  const { photos } = useGallery();
  const { userInfo } = useAuth();

  const addClientMobileNumber = number => setClientMobileNumber(number);

  const clearResult = () => {
    setClientMobileNumber(null);
    setResult(null);
  };

  const createResult = () => {
    const photoUrls = photos.map(p => {
      const { publicUrl } = p;
      if (publicUrl) return publicUrl;
      throw new ValidationError({
        code: 1014,
        message: 'Error happened when trying to create result from available data.',
      });
    });
    const theResult = { clientMobileNumber, photos: photoUrls };
    setResult(theResult);
    return theResult;
  };

  const submitResult = async (signal = null) => {
    const theResult = createResult();
    const serverResult = await api.addResult(
      {
        result: theResult,
        userInfo,
      },
      signal,
    );
    return serverResult;
  };

  const contextValues = {
    addClientMobileNumber,
    clientMobileNumber,
    submitResult,
    clearResult,
    result,
  };
  return <ResultContext.Provider value={contextValues}>{children}</ResultContext.Provider>;
};

ResultProvider.propTypes = {
  children: PropTypes.node,
};

ResultProvider.defaultProps = {
  children: null,
};

export { ResultContext, ResultConsumer, ResultProvider, useResult };
