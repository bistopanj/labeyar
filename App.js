import React, { useState, useEffect } from 'react';
import { Text } from 'native-base';
import codePush from 'react-native-code-push';
import SplashScreen from 'react-native-splash-screen';
import { useTranslation } from 'react-i18next';
import AppContainer from './src/nav/AppContainer';
import { setTopLevelNavigator } from './src/services/nav.service';
import { translateForUser } from './src/errors';
import { timing } from './src/constants';
import { findDefaultAppLang } from './src/services/lang.service';
import { AppContextProvider } from './src/contexts';
import FullPageContainer from './src/components/FullPageContainer';

const App = () => {
  const [isAppReady, setIsAppReady] = useState(false);
  const [minSplashTimePassed, setMinSplashTimePassed] = useState(false);
  const [defaultLang, setDefaultLang] = useState('en');
  const [errorMessage, setErrorMessage] = useState(null);
  const [loadApp, setLoadApp] = useState(false);

  const { t } = useTranslation();

  useEffect(() => {
    const splashTimer = setTimeout(() => {
      setMinSplashTimePassed(true);
    }, timing.minSplash);

    return () => {
      clearTimeout(splashTimer);
    };
  }, []);

  const cacheResourcesAsync = async () => {
    try {
      // TODO: do all async ops here
      const lang = await findDefaultAppLang();
      setDefaultLang(lang);
      setIsAppReady(true);
    } catch (error) {
      setErrorMessage(t(translateForUser(error)));
    }
  };

  useEffect(() => {
    cacheResourcesAsync();
  }, []);

  useEffect(() => {
    if (isAppReady && minSplashTimePassed) {
      SplashScreen.hide();
      setLoadApp(true);
    }
  }, [isAppReady, minSplashTimePassed]);

  return loadApp ? (
    <AppContextProvider defaultLang={defaultLang}>
      <AppContainer
        ref={navRef => {
          setTopLevelNavigator(navRef);
        }}
      />
    </AppContextProvider>
  ) : (
    <FullPageContainer>{errorMessage && <Text>{errorMessage}</Text>}</FullPageContainer>
  );
};

const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.ON_NEXT_RESUME,
};

export default codePush(codePushOptions)(App);
