module.exports = {
  // "jsxBracketSameLine": true,
  // "bracketSpacing": false,
  printWidth: 100,
  trailingComma: 'all',
  tabWidth: 2,
  semi: true,
  singleQuote: true,
};
