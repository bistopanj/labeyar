module.exports = {
  project: {
    ios: {},
    android: {},
  },
  dependencies: {
    'react-native-code-push': {
      platforms: {
        android: null,
      },
    },
  },
  assets: ['./assets/fonts'],
};
